package proman.audio;


import static org.lwjgl.openal.AL10.*;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

import proman.core.ManagerException;

public class SoundSource 
{
	int id;

	private float gain;
	private float pitch;
	
	private Sound sound;
	
	FloatBuffer posBuffer = BufferUtils.createFloatBuffer(3);
	
	public SoundSource()
	{
		this.id = alGenSources();
		Audio.sources.add(this);
	}
	
	public SoundSource(Sound sound) throws ManagerException
	{
		this();
		if(sound != null)
		{
			this.sound = sound;
			alSourcei(this.id, AL_BUFFER, this.sound.getID());
		}
	}
	
		
	public boolean isPlaying()					{	return alGetSourcei(this.id, AL_SOURCE_STATE) == AL_PLAYING;}
	
	public void play()							{	/*alSourcePlay(this.getId());*/}
	public void pause()							{	alSourcePause(this.id);}
	public void stop()							{	alSourceStop(this.id);}
	public void destroy()						{	alDeleteSources(this.id);} 
	
	public void setGain(float gain) 			{	this.gain = gain;		alSourcef(	this.id, AL_GAIN, gain);}
	public void setPitch(float pitch)			{	this.pitch = pitch;		alSourcef(	this.id, AL_PITCH, pitch);}
	public void setLooping(boolean looping)		{	alSourcei(	this.id, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);	}
	public void setSound(Sound sound)			{	this.sound = sound;		alSourcei(	this.id, AL_BUFFER, sound.getID());}
	
	public float getGain()						{	return this.gain;}
	public float getPitch()						{	return this.pitch;}
	public Sound getSound()						{	return this.sound;}
	public boolean isLooping()					{	return alGetSourcef(this.id, AL_LOOPING) == AL_TRUE;}
	
	public void setPosition(float x, float y, float z)
	{
		this.posBuffer.rewind();
		this.posBuffer.put(x).put(y).put(z).rewind();
		alSourcefv(this.id, AL_POSITION, this.posBuffer);

	}
	

	public int getId()							{	return this.id;}
}
