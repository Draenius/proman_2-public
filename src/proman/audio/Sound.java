package proman.audio;

import proman.util.io.Address;
import proman.core.ManagerException;
import static org.lwjgl.openal.AL10.*;

public class Sound 
{

	private int id;
	
	public Sound(Address address)
	{
        this.id = alGenBuffers();
	}
	
	int getID()
	{
		return this.id;
	}
	
	/** Calls Audio.playSound() with
	 * @param this as sound
	 * @param gain as gain*/
	public void playDirectly(float gain) throws ManagerException
	{
		Audio.playSound(this, gain);
	}
}
