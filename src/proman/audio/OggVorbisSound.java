package proman.audio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.stb.STBVorbis;
import org.lwjgl.stb.STBVorbisInfo;

import proman.util.io.Address;

public class OggVorbisSound extends Sound
{
	public OggVorbisSound(Address source)
	{
		super(source);
        checkALError();
        
		try(STBVorbisInfo info = STBVorbisInfo.malloc())
		{
			
			ByteBuffer vorbis;
			try
			{
				InputStream stream = source.openInputStream();
				byte[] array = new byte[stream.available()];
				stream.read(array);
				vorbis = ByteBuffer.wrap(array);
				
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}

			IntBuffer error = BufferUtils.createIntBuffer(1);
			long decoder = STBVorbis.stb_vorbis_open_memory(vorbis, error, null);
			if(decoder == 0)
			{
				throw new RuntimeException(
						"Failed to open Ogg Vorbis file. Error: "
								+ error.get(0));
			}

			STBVorbis.stb_vorbis_get_info(decoder, info);

			int channels = info.channels();

			int lengthSamples = STBVorbis.stb_vorbis_stream_length_in_samples(decoder);

			ShortBuffer pcm = BufferUtils.createShortBuffer(lengthSamples);

			pcm.limit(STBVorbis.stb_vorbis_get_samples_short_interleaved(decoder,
					channels, pcm) * channels);
			STBVorbis.stb_vorbis_close(decoder);
	
			AL10.alBufferData(this.getID(), 
					info.channels() == 1 ? AL10.AL_FORMAT_MONO16 : AL10.AL_FORMAT_STEREO16, 
					pcm, info.sample_rate());
			
			checkALError();
			
			
		}
		
		
	}
	
	private void checkALError()
	{
		if(AL10.alGetError() != AL10.AL_NO_ERROR)
			System.err.println("OpenAL error");
	}
}
