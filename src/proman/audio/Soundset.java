package proman.audio;

import java.util.HashMap;

public class Soundset 
{
	private static HashMap<String, Sound> sounds = new HashMap<String, Sound>();
	
	/** Adds a new sound to the set
	 * @param sound - the sound to be added
	 * @param id - the ID to register sound on*/
	public static void addSound(Sound sound, String id)
	{
		sounds.put(id, sound);
	}
	
	/** Returns the sound registered at ID
	 * @param id */
	public static Sound get(String id)
	{
		return sounds.get(id);
	}
}
