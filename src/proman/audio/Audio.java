package proman.audio;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.LinkedList;
import java.util.Queue;

import proman.core.ManagerException;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALC10;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.EXTThreadLocalContext;

import static org.lwjgl.openal.AL10.*;

public class Audio 
{
	
	static LinkedList<Sound> sounds = new LinkedList<Sound>();
	static LinkedList<SoundSource> sources = new LinkedList<SoundSource>();
	
	static LinkedList<SoundSource> directSounds = new LinkedList<SoundSource>();
	
	static Queue<SoundSource> sourcesToInitiate = new LinkedList<SoundSource>();
	
	static boolean alCreated;
	
	static FloatBuffer vec3Buffer = BufferUtils.createFloatBuffer(3);
	
	
	private static long alDevice, alContext;
	
	/** Initiates the OpenAL context. Automatically called by Core.initiate()*/
	public static void create() throws ManagerException
	{
		if(alCreated) return;
		
		try
		{
			long device = ALC10.alcOpenDevice((ByteBuffer)null);
			if(device == 0)
				throw new ManagerException("Failed to open OpenAL device");
			
			ALCCapabilities deviceCaps = ALC.createCapabilities(device);
			
			System.out.println("Using audio device: \""+
					ALC10.alcGetString(device, ALC10.ALC_DEFAULT_DEVICE_SPECIFIER) +"\"");
			
			long context = ALC10.alcCreateContext(device, (IntBuffer)null);
			EXTThreadLocalContext.alcSetThreadContext(context);
			AL.createCapabilities(deviceCaps);
			
			alDevice = device;
			alContext = context;
			
			alCreated = true;
		}
		catch(Exception e)
		{
			throw new ManagerException("Unable to initiate OpenAL: "+e.getLocalizedMessage());
		}
	}
	
	/** Destroys the OpenAL context. Automatically called by Core.destroy()*/
	public static void destroy()
	{
		if(!alCreated) return;
		
		for(Sound sound : sounds)
			alDeleteBuffers(sound.getID());
		for(SoundSource source : sources)
			alDeleteSources(source.getId());
		ALC10.alcMakeContextCurrent(0);
		ALC10.alcDestroyContext(alContext);
		ALC10.alcCloseDevice(alDevice);
		
		alCreated = false;
	}
	
	/** Removes unused and unhandled sound sources to avoid overload*/
	public static void update()
	{
		if(!alCreated) return;
		
		for(int i=0;i<directSounds.size();i++)
			if(!directSounds.get(i).isPlaying())
			{
				System.out.println("deleted"+directSounds.get(i).id);
				directSounds.get(i).destroy();
				directSounds.remove(i);
				i--;
			}
	}
	
	/** Plays a sound at the given gain directly without any spatial properties
	 * @param sound  - the sound to be played
	 * @param gain  - the gain to play sound at*/
	public static void playSound(Sound sound, float gain) throws ManagerException
	{
		playSound(sound, gain, 1);
	}
	
	/** Plays a sound at the given gain directly without any spatial properties
	 * @param sound - the sound to be played
	 * @param gain - the gain to play sound at
	 * @param pitch - the pitch to play the sound at*/
	public static void playSound(Sound sound, float gain, float pitch) throws ManagerException
	{
		if(!alCreated) throw new ManagerException("Audio not created");
		SoundSource source = new SoundSource(sound);
		source.setGain(gain);
		source.setPitch(pitch);
		directSounds.add(source);
		source.play();
		System.out.println(source.id);
	}
	
	public static void setListenerPosition(float x, float y, float z)
	{
		vec3Buffer.rewind();
		vec3Buffer.put(x).put(y).put(z).rewind();
		alListenerfv(AL_POSITION, vec3Buffer);
	}
	
}
