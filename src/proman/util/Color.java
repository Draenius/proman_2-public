package proman.util;

import proman.math.vector.Vec4f;


public class Color extends Vec4f
{
	public Color()
	{
		super(1, 1, 1, 1);
	}
	
	public Color(float red, float green, float blue)
	{
		super(red, green, blue, 1);
	}
	
	public Color(float red, float green, float blue, float alpha)
	{
		super(red, green, blue, alpha);
	}
	
	public Color(Vec4f color)
	{
		super(color.x, color.y, color.z, color.w);
	}
	
	public Color(Color rgb, float alpha)
	{
		super(rgb.x, rgb.y, rgb.z, alpha);
	}
	
	public Color dim(float factor)
	{
		return new Color(this.x*factor, this.y*factor, this.z*factor, this.w);
	}
	
	public Color dim(float redFactor, float greenFactor, float blueFactor)
	{
		return new Color(this.x*redFactor, this.y*greenFactor,  this.z*blueFactor, this.w);
	}
		
	public void setRed(float red)		{	this.x = red;}
	public void setGreen(float green)	{	this.y = green;}
	public void setBlue(float blue)		{	this.z = blue;}
	
	public float getRed()				{	return this.x;}
	public float getGreen()				{	return this.y;}
	public float getBlue()				{	return this.z;}
	public float getAlpha()				{	return this.w;}
	
	public Color clone()
	{
		return new Color(this.x, this.y, this.z, this.w);
	}
	
	public static final Color BLACK = new Color(0f, 0f, 0f);
	public static final Color WHITE = new Color(1f, 1f, 1f);
	public static final Color RED = new Color(1f, 0f, 0f);
	public static final Color GREEN = new Color(0f, 1f, 0);
	public static final Color PURPLE = new Color(1f, 0f, 1f);
	public static final Color DARK_GREEN = new Color(0f, 0.5f, 0.125f);
	public static final Color BLUE = new Color(0f, 0f, 1f);
	public static final Color LIGHT_BLUE = new Color(0f, 0.5f, 1f);
	public static final Color DARK_BLUE = new Color(0f, 0f, 0.5f);
	public static final Color YELLOW = new Color(1f, 1f, 0f);
	public static final Color ORANGE = new Color(1f, 0.5f, 0f);
	public static final Color MEDIUM_BLUE = new Color(1f, 0.5f, 0f);
	public static final Color CYAN = new Color(0f, 0.5f, 0.5f);
	public static final Color GRAY = new Color(0.5f, 0.5f, 0.5f);
	public static final Color LIGHT_GRAY = new Color(0.75f, 0.75f, 0.75f);
	public static final Color DARK_GRAY = new Color(0.25f, 0.25f, 0.25f);
	public static final Color BROWN = new Color(0.5f, 0.15f, 0);
	public static final Color DARK_BROWN = new Color(0.05f, 0.015f, 0);
	
}
