package proman.util;

import proman.math.vector.Vec2;
import proman.math.vector.Vec2d;
import proman.math.vector.Vec2f;

public class VecUtil
{
	/** Returns the degree angle required to point from one point to another*/
	public static float relativeAngle(Vec2<?> from, Vec2<?> to)
	{
		Vec2<?> dis = to.sub(from);
		Vec2d doubleDis = new Vec2d(1, 1).mul(dis);
		double distance = MathUtil.sqrt(doubleDis.x*doubleDis.x+doubleDis.y*doubleDis.y);
		double deltaAngle = MathUtil.asin((float) ((doubleDis.x)/distance));
		
		float returnValue;
		if(doubleDis.y >= 0) returnValue = (float)((deltaAngle+360d)%360d);
		else returnValue = (float)(90.0+(90.0-deltaAngle));
		
		return (Float.isNaN(returnValue) ? 0 : returnValue);
	}
	
	/** Returns whether or not a point is within a certain range to another*/
	public static boolean disInRange(Vec2<?> pos1, Vec2<?> pos2, float distance)
	{
		Vec2<?> dis = pos1.sub(pos2);
		Vec2d doubleDis = new Vec2d(1, 1).mul(dis);
		return doubleDis.x*doubleDis.x+doubleDis.y*doubleDis.y < distance*distance;
	}
	
	/** Returns the distance from one point to another*/
	public static float distance(Vec2<?> pos1, Vec2<?> pos2)
	{
		Vec2<?> dis = pos1.sub(pos2);
		Vec2d doubleDis = new Vec2d(1, 1).mul(dis);
		return (float)Math.sqrt(doubleDis.x*doubleDis.x+doubleDis.y*doubleDis.y);
	}
	
	/** Returns the vectorized direction the vector is pointing to*/
	public static Vec2f normalize(Vec2<?> vector)
	{
		Vec2f vec2f = new Vec2f(1,1).mul(vector);
		float dis = MathUtil.positive(vec2f.x)+MathUtil.positive(vec2f.y);
		return new Vec2f(vec2f.x/dis, vec2f.y/dis);
	}
	
	/** Returns a normalized vector pointing into an angle*/
	public static Vec2f vectorize(float angle)
	{
		return new Vec2f(MathUtil.sin(angle), MathUtil.cos(angle));
	}

}
