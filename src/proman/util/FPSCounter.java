package proman.util;

import java.util.LinkedList;

import proman.time.ProgressTimer;

public class FPSCounter
{
	LinkedList<ProgressTimer> timers = new LinkedList<ProgressTimer>();
	
	public void tick()
	{
		timers.add(new ProgressTimer(1));
	}
	
	public int getFPS()
	{
		while(!timers.isEmpty() && timers.getFirst().completed())
			timers.removeFirst();
		return timers.size();
	}
}
