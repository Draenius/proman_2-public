package proman.util;

public class MathUtil 
{
	
	/** Converts a random angle degree number into its equivalent position between 0� and 360�*/
	public static float toDegreeAngle(float angle)
	{
		return angle >= 0 ? angle%360f : 360f-angle%360f;
	}
	
	/** Returns the next integer number smaller than the input*/
	public static int floorOf(float value)
	{
		return value >= 0 ? (int) value : (int) value - 1;
	}
	
	/** Returns the next integer number smaller than the input*/
	public static int floorOf(double value)		{	return value >= 0 ? (int) value : (int) value - 1;}
	
	/** Returns the sine of a degree angle*/
	public static float sin(float angle)			{	return (float) Math.sin(Math.toRadians(angle));}
	
	/** Returns the cosine of a degree angle*/
	public static float cos(float angle)			{	return (float) Math.cos(Math.toRadians(angle));}
	
	/** Returns the tangent of a degree angle*/
	public static float tan(float angle)			{	return (float) Math.tan(Math.toRadians(angle));}
	
	/** Returns the arc sine degree angle of a number*/
	public static float asin(float arc)				{	return (float) Math.toDegrees(Math.asin(arc));}
	
	/** Returns the arc cosine degree angle of a number*/
	public static float acos(float arc)				{	return (float) Math.toDegrees(Math.acos(arc));}
	
	/** Returns the arc tangent degree angle of a number*/
	public static float atan(float arc)				{	return (float) Math.toDegrees(Math.atan(arc));}
	
	/** Returns the logarithm of a source to a base*/
	public static float log(float src, float base)	{	return (float)(Math.log(src)/Math.log(base));}
	
	/** Returns the x'th power of a number*/
	public static float potency(float value, float potency)
	{
		if(potency == 0)
			return 1;
		
		float returnValue = value;
		if(value >= 0)
		{
			for(int i=1;i<potency;i++)
				returnValue *= value;
			return returnValue;
		}
		else
		{
			return value;
		}
	}
	
	/** Returns the positive value of a number*/
	public static float positive(float value)		{	return value >= 0 ? value : -value;}
	/** Returns 1 by the input's sign or 0 for 0*/
	public static float pol(float value)			{	return value == 0 ? 0 : value > 0f ? 1f : -1f;}
	/** Returns the bigger of two numbers*/
	public static float max(float opt1, float opt2)	{	return opt1>=opt2 ? opt1 : opt2;}
	/** Returns the smaller of two numbers*/
	public static float min(float opt1, float opt2)	{	return opt1<opt2 ? opt1 : opt2;}
	
	/** Returns the positive value of a number*/
	public static double positive(double value)		{	return value > 0 ? value : (value == 0 || value == -0 ? 0 : -value);}
	/** Returns the bigger of two numbers*/
	public static double max(double opt1, double opt2)	{	return opt1>=opt2 ? opt1 : opt2;}
	/** Returns the smaller of two numbers*/
	public static double min(double opt1, double opt2)	{	return opt1<opt2 ? opt1 : opt2;}
	
	/** Returns 1 by the input's sign or 0 for 0*/	
	public static int polarity(double value)		{	return value == 0 ? 0 : value > 0f ? 1 : -1;}
	
	/** Splits a number into smaller heaps of maximum size maxHeap*/
	public static float[] split(float value, float maxHeap)
	{
		int fullHeaps = (int)(value/maxHeap);
		float[] returnValue = new float[fullHeaps+(value%maxHeap>0?1:0)];
		for(int i=0;i<fullHeaps;i++)
			returnValue[i] = maxHeap;
		if(value%maxHeap > 0)
			returnValue[returnValue.length-1] = value%maxHeap;
		return returnValue;
	}
	
	/** Returns 1 if a number is positive, or 0*/
	public static float positivity(float value)
	{
		return value > 0 ? 1f : 0f;
	}
	
	/** Returns 1 if a number is negative, or 0*/
	public static float negativity(float value)
	{
		return value < 0 ? 1f : 0f;
	}
	
	/** Returns positive(to-from)*/
	public static float difference(float from, float to)
	{
		return positive(to-from);
	}
	
	/** Returns the square root of a number*/
	public static float sqrt(float src)
	{
		return (float)Math.sqrt(src);
	}
	
	/** Returns the square root of a number*/
	public static double sqrt(double src)
	{
		return Math.sqrt(src);
	}
	
	/** Returns the shorter rotation direction to get from one rotation angle to another*/
	public static float shortestAngle(float from, float to)
	{
		from %= 360f;	while(from<0) from += 360f;
		to %= 360f;	while(to<0) to += 360f;
		
		if(from >= to)
		{
			if((from-to) <= 180)	return -(from-to);
			else					return (360f-(from-to));
		}
		else
		{
			if((to-from) <= 180)	return to-from;
			else					return -(360f-(to-from));

		}
	}
	
}
