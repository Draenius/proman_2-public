package proman.util;

import java.io.File;
import java.io.PrintStream;

import proman.util.io.SplitPrintStream;

public class Logger 
{
	public static void log(Object o)
	{
		System.out.println(o);
	}
	
	public static void log()
	{
		System.out.println();
	}
	
	public static void err(Object o)
	{
		System.out.println(o);
	}
	
	public static void logIntoFile(File file)
	{
		try
		{
			System.setOut(new SplitPrintStream(new PrintStream[]{System.out, new PrintStream(file)}));
		}catch(Exception e){}
	}
	
	public static void drawBitStream(String bits)
	{
		char[] chars = bits.toCharArray();
		String[] bytes = new String[chars.length/8+1];
		for(int i=0;i<bytes.length;i++)
			bytes[i] = "";
		for(int i=0;i<chars.length;i++)
			bytes[i/8] += chars[i];
		
		String finalString = "";
		for(int b=0;b<bytes.length;b++)
		{
			int value = 0;
			
			for(char ch : bytes[b].toCharArray())
			{
				value *= 2;
				value += ch == '1' ? 1 : 0;
			}
			finalString += (char)value;
				
		}
		Logger.log(finalString);
			
	}
}
