package proman.util;

import proman.math.vector.Vec2d;
import proman.math.vector.Vec2f;

public class Geometry 
{
	public static boolean pointInShape(Vec2f point, Vec2f[] shape)
	{
		for(int i=0;i<shape.length;i++)
		{
			Vec2f edge0 = shape[i];
			Vec2f edge1 = shape[(i+1)%shape.length];
			
			if((point.x*edge1.y-edge1.x*point.y)-(point.x*edge0.y-edge0.x*point.y)+(edge1.x*edge0.y-edge0.x*edge1.y) > 0)
				return false;
		}
		
		return true;
	}
	
	
	public static boolean pointInShape(Vec2d point, Vec2d[] shape)
	{
		for(int i=0;i<shape.length;i++)
		{
			Vec2d edge0 = shape[i];
			Vec2d edge1 = shape[(i+1)%shape.length];
			
			if((point.x*edge1.y-edge1.x*point.y)-(point.x*edge0.y-edge0.x*point.y)+(edge1.x*edge0.y-edge0.x*edge1.y) > 0)
				return false;
		}
		
		return true;
	}
}
