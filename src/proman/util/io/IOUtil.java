package proman.util.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import proman.core.ManagerException;

public class IOUtil 
{
	public static void copyFile(Address input, String output) throws ManagerException
	{
		InputStream in;
		OutputStream out;
		try
		{
			in = input.openInputStream();
			if(in == null) throw new Exception();
			
			try
			{
				File outputFile = new File(output);
				if(!outputFile.isFile())
					outputFile.mkdirs();
				
				out = new FileOutputStream(output);
				
				byte[] buffer = new byte[65536];
		        int bufferSize;
		        while ((bufferSize = in.read(buffer, 0, buffer.length)) != -1)
		        {
		        	out.write(buffer, 0, bufferSize);
		        }
		
		        in.close();
		        out.close();
			}
			catch(Exception e)
			{
				throw new ManagerException(	"Unable to write to file \""+output+"\""+ 
											(": "+e.getMessage()).replaceAll(": null", ""));
			}
		}
		catch(ManagerException e)
		{
			throw e;
		}	
		catch(Exception e)
		{
			throw new ManagerException("Unable to copy file: Unable to read source file \""+input.getPath()+"\n"+
											(": "+e.getMessage()).replaceAll(": null", ""));
		}
	}
	
	public static String readFile(Address adress) 
	{
		BufferedReader loader;
		try
		{
			InputStream is = adress.openInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			loader = new BufferedReader(isr);
			String line;
			String content = "";
			 while ((line = loader.readLine()) != null)
			 {
				 String string = line.intern();
				 content += string+'\n';
			 }
			 
			 is.close();
			 return content;
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	
	public static InputStream load(String path)
	{
		return new Address(path).openInputStream();
	}
	
	public static ByteBuffer readIntoBuffer(Address address) throws Exception
	{
		ByteBuffer buffer;
		InputStream stream = address.openInputStream();
		byte[] bytes = new byte[stream.available()]; 
		stream.read(bytes);
		buffer = ByteBuffer.wrap(bytes);
		buffer.rewind();
		return buffer;
	}
}
