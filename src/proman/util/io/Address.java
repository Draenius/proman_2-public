package proman.util.io;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

public class Address 
{
	private String value;
	
	public Address(String adress)
	{
		this.value = adress;
	}
	
	public InputStream openInputStream()
	{
		try
		{
			if(this.value.startsWith("res://"))
				return ClassLoader.getSystemResourceAsStream(this.value.replaceFirst("res://", ""));
			else
				return new URL(this.value).openStream();
		}
		catch(Exception e)
		{
			return new ByteArrayInputStream(new byte[0]);
		}
	}
	
	public String getPath()
	{
		return this.value;
	}
}
