package proman.util.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileInput
{
	ByteBuffer buffer;
	
	public FileInput(String dir, String file) throws IOException
	{
		byte[] bytes = Files.readAllBytes(Paths.get(dir+file));
		this.buffer = ByteBuffer.wrap(bytes);
		this.buffer.rewind();
	}
	
	public byte readByte()		{	return this.buffer.get();}
	public short readShort()	{	return this.buffer.getShort();}
	public int readInt()		{	return this.buffer.getInt();}
	public float readFloat()	{	return this.buffer.getFloat();}
	public long readLong()		{	return this.buffer.getLong();}
	public double readDouble()	{	return this.buffer.getDouble();}
	
	public void reset()			{	this.buffer.rewind();}
	public void setPosition(int position)	{	this.buffer.position(position);}
	
	public int position()		{	return this.buffer.position();}
	public int length()			{	return this.buffer.capacity();}
}
