package proman.util.io;

import java.io.PrintStream;

public class SplitPrintStream extends PrintStream
{
	PrintStream[] streams;
	
	public SplitPrintStream(PrintStream out) 	
	{		
		super(out);
		this.streams = new PrintStream[]{ out};
	}
	
	public SplitPrintStream(PrintStream[] outputs)
	{
		super(outputs[0]);
		this.streams = outputs;
	}
	
	public void println(Object out)		
	{	
		for(PrintStream stream : this.streams)	
			stream.println(out);
	}
	
	public void print(Object out)
	{
		for(PrintStream stream : this.streams)
			stream.print(stream);
	}
	
	
}
