package proman.util.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class FileOutput
{
	FileOutputStream stream;
	ByteBuffer buffer;
	
	private boolean available = false;
	
	public FileOutput(String dir, String file) throws IOException
	{
		File dirFile = new File(dir);
		if(!dirFile.isDirectory())
		{
			dirFile.mkdirs();
		}
		File fileFile = new File(dir+file);
		if(fileFile.isFile())
		{
			fileFile.delete();
			fileFile.createNewFile();
		}
		
		this.stream = new FileOutputStream(dir+file);
		this.buffer = ByteBuffer.allocate(128);
		
		this.available = true;
		
	}
	
	public void writeByte(byte out)		{	if(this.available) {this.buffer.put(out); 		this.update();}}
	public void writeShort(short out)	{	if(this.available) {this.buffer.putShort(out); 	this.update();}}
	public void writeInt(int out)		{	if(this.available) {this.buffer.putInt(out); 	this.update();}}
	public void writeFloat(float out)	{	if(this.available) {this.buffer.putFloat(out); 	this.update();}}
	public void writeLong(long out)		{	if(this.available) {this.buffer.putLong(out); 	this.update();}}
	public void writeDouble(double out)	{	if(this.available) {this.buffer.putDouble(out); this.update();}}
	
	public void close() throws IOException
	{
		if(!this.available) return;
		writeFromBufferToStream(this.buffer.position());
		
		this.stream.flush();
		this.stream.close();
		this.available = false;

	}
	
	private void update()
	{
		try
		{
			int position = this.buffer.position();
			if(position >= this.buffer.capacity()-Double.BYTES)
			{
				writeFromBufferToStream(position);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
		
	private void writeFromBufferToStream(int limit) throws IOException
	{
		byte[] array = new byte[limit];
		this.buffer.rewind();
		for(int i=0;i<limit;i++)
			array[i] = this.buffer.get();
		this.stream.write(array);
		this.buffer.rewind();
	}

}
