package proman.math.vector;

public class Vec1i implements Vec1<Integer>
{
	public int x;
	
	@Override
	public Integer getX() 		{	return this.x;}
	@Override
	public void setX(Integer x) {	this.x = x;}
	
	public Vec1i(int x)
	{
		this.x = x;
	}
	
	@Override
	public Vec1i add(Vec1<?> add) {
		if(compatibleType(add.getX()))
			return new Vec1i(
					this.x + intValue(add.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1i sub(Vec1<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec1i(
					this.x - intValue(sub.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1i mul(Vec1<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec1i(
					this.x * intValue(mul.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1i div(Vec1<?> div) {
		if(compatibleType(div.getX()))
			return new Vec1i(
					this.x / intValue(div.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1i clone() {
		return new Vec1i(this.x);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec1<?>)
			return compatibleType(( (Vec1<?>)o ).getX()) ? ( (Vec1<?>)o ).getX().equals(this.getX()) : false;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.x;
	}
	
	@Override
	public String toString() {
		return this.x+"";
	}
	
	public static int intValue(Object o)
	{
			 if(o instanceof Byte)		return ((Byte)o).intValue();
		else if(o instanceof Short)		return ((Short)o).intValue();
		else if(o instanceof Integer)	return ((Integer)o).intValue();
		else if(o instanceof Float)		return ((Float)o).intValue();
		else if(o instanceof Long)		return ((Long)o).intValue();
		else if(o instanceof Double)	return ((Double)o).intValue();
		else return 0;
	}
	
	public static boolean compatibleType(Object o)
	{
		return o instanceof Byte || o instanceof Short || o instanceof Integer || o instanceof Float || o instanceof Long || o instanceof Double;
	}
}
