package proman.math.vector;

public class Vec2d extends Vec1d implements Vec2<Double>
{
	public double y;
	
	public Vec2d(double x, double y) {
		super(x);
		this.y = y;
	}


	@Override
	public Double getY() {
		return this.y;
	}

	@Override
	public void setY(Double y) {
		this.y = y;
	}

	@Override
	public Vec2d add(Vec2<?> add) {
		if(compatibleType(add.getX()))
			return new Vec2d(
					this.x + doubleValue(add.getX()),
					this.y + doubleValue(add.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2d sub(Vec2<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec2d(
					this.x - doubleValue(sub.getX()),
					this.y - doubleValue(sub.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2d mul(Vec2<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec2d(
					this.x * doubleValue(mul.getX()),
					this.y * doubleValue(mul.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2d div(Vec2<?> div) {
		if(compatibleType(div.getX()))
			return new Vec2d(
					this.x / doubleValue(div.getX()),
					this.y / doubleValue(div.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2d clone() {
		return new Vec2d(this.x, this.y);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec2<?>)
			return compatibleType(( (Vec2<?>)o ).getX()) ? ( (Vec2<?>)o ).getX().equals(this.getX()) && ( (Vec2<?>)o ).getY().equals(this.getY()) : false;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.x+ (int)this.y;
	}
	
	@Override
	public String toString() {
		return this.x+"|"+this.y;
	}
}
