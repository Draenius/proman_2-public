package proman.math.vector;

public interface Vec1<T> extends Vec<T>
{
	public abstract T getX();
	public abstract void setX(T x);
	
	public abstract Vec1<T> add(Vec1<?> add);
	public abstract Vec1<T> sub(Vec1<?> sub);
	public abstract Vec1<T> mul(Vec1<?> mul);
	public abstract Vec1<T> div(Vec1<?> div);
	
	public abstract Vec1<T> clone();
	public abstract int hashCode();
	public abstract boolean equals(Object obj);
}
