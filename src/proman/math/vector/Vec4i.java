package proman.math.vector;

public class Vec4i extends Vec3i implements Vec4<Integer>
{
	public int w;
	
	public Vec4i(int x, int y, int z, int w) {
		super(x, y, z);
		this.w = w;
	}


	@Override
	public Integer getW() {
		return this.w;
	}

	@Override
	public void setW(Integer w) {
		this.w = w;
	}

	@Override
	public Vec4i add(Vec4<?> add) {
		if(compatibleType(add.getX()))
			return new Vec4i(
					this.x + intValue(add.getX()),
					this.y + intValue(add.getY()),
					this.z + intValue(add.getZ()),
					this.w + intValue(add.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4i sub(Vec4<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec4i(
					this.x - intValue(sub.getX()),
					this.y - intValue(sub.getY()),
					this.z - intValue(sub.getZ()),
					this.w - intValue(sub.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4i mul(Vec4<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec4i(
					this.x * intValue(mul.getX()),
					this.y * intValue(mul.getY()),
					this.z * intValue(mul.getZ()),
					this.w * intValue(mul.getW()));
		else
			return this.clone();
	}
	
	@Override
	public Vec4i div(Vec4<?> div) {
		if(compatibleType(div.getX()))
			return new Vec4i(
					this.x / intValue(div.getX()),
					this.y / intValue(div.getY()),
					this.z / intValue(div.getZ()),
					this.w / intValue(div.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4i clone() {
		return new Vec4i(this.x, this.y, this.z, this.w);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec4<?>)
			return compatibleType(( (Vec4<?>)o ).getX()) ? ( (Vec4<?>)o ).getX().equals(this.getX()) && ( (Vec4<?>)o ).getY().equals(this.getY()) && ( (Vec4<?>)o ).getZ().equals(this.getZ())  && ( (Vec4<?>)o ).getW().equals(this.getW()): false;
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.x + (int)this.y + (int)this.z + (int)this.w;
	}
	
	
	@Override
	public String toString() {
		return this.x+"|"+this.y+"|"+this.z+"|"+this.w;
	}
}
