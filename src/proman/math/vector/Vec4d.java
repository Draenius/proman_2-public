package proman.math.vector;

public class Vec4d extends Vec3d implements Vec4<Double>
{
	public double w;
	
	public Vec4d(double x, double y, double z, double w) {
		super(x, y, z);
		this.w = w;
	}


	@Override
	public Double getW() {
		return this.w;
	}

	@Override
	public void setW(Double w) {
		this.w = w;
	}

	@Override
	public Vec4d add(Vec4<?> add) {
		if(compatibleType(add.getX()))
			return new Vec4d(
					this.x + doubleValue(add.getX()),
					this.y + doubleValue(add.getY()),
					this.z + doubleValue(add.getZ()),
					this.w + doubleValue(add.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4d sub(Vec4<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec4d(
					this.x - doubleValue(sub.getX()),
					this.y - doubleValue(sub.getY()),
					this.z - doubleValue(sub.getZ()),
					this.w - doubleValue(sub.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4d mul(Vec4<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec4d(
					this.x * doubleValue(mul.getX()),
					this.y * doubleValue(mul.getY()),
					this.z * doubleValue(mul.getZ()),
					this.w * doubleValue(mul.getW()));
		else
			return this.clone();
	}
	
	@Override
	public Vec4d div(Vec4<?> div) {
		if(compatibleType(div.getX()))
			return new Vec4d(
					this.x / doubleValue(div.getX()),
					this.y / doubleValue(div.getY()),
					this.z / doubleValue(div.getZ()),
					this.w / doubleValue(div.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4d clone() {
		return new Vec4d(this.x, this.y, this.z, this.w);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec4<?>)
			return compatibleType(( (Vec4<?>)o ).getX()) ? ( (Vec4<?>)o ).getX().equals(this.getX()) && ( (Vec4<?>)o ).getY().equals(this.getY()) && ( (Vec4<?>)o ).getZ().equals(this.getZ())  && ( (Vec4<?>)o ).getW().equals(this.getW()): false;
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.x + (int)this.y + (int)this.z + (int)this.w;
	}
	
	
	@Override
	public String toString() {
		return this.x+"|"+this.y+"|"+this.z+"|"+this.w;
	}
}
