package proman.math.vector;

public class Vec3i extends Vec2i implements Vec3<Integer>
{
	public int z;
	
	public Vec3i(int x, int y, int z) {
		super(x, y);
		this.z = z;
	}


	@Override
	public Integer getZ() {
		return this.z;
	}

	@Override
	public void setZ(Integer z) {
		this.z = z;
	}

	@Override
	public Vec3i add(Vec3<?> add) {
		if(compatibleType(add.getX()))
			return new Vec3i(
					this.x + intValue(add.getX()),
					this.y + intValue(add.getY()),
					this.z + intValue(add.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3i sub(Vec3<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec3i(
					this.x - intValue(sub.getX()),
					this.y - intValue(sub.getY()),
					this.z - intValue(sub.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3i mul(Vec3<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec3i(
					this.x * intValue(mul.getX()),
					this.y * intValue(mul.getY()),
					this.z * intValue(mul.getZ()));
		else
			return this.clone();
	}
	
	@Override
	public Vec3i div(Vec3<?> div) {
		if(compatibleType(div.getX()))
			return new Vec3i(
					this.x / intValue(div.getX()),
					this.y / intValue(div.getY()),
					this.z / intValue(div.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3i clone() {
		return new Vec3i(this.x, this.y, this.z);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec3<?>)
			return compatibleType(( (Vec3<?>)o ).getX()) ? ( (Vec3<?>)o ).getX().equals(this.getX()) && ( (Vec3<?>)o ).getY().equals(this.getY()) && ( (Vec3<?>)o ).getZ().equals(this.getZ()): false;
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.x + (int)this.y + (int)this.z;
	}


}
