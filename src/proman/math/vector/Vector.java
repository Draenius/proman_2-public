package proman.math.vector;

public class Vector 
{
	public static Vec1f vec1f(double x)									{	return new Vec1f((float)x);}
	public static Vec2f vec2f(double x, double y)						{	return new Vec2f((float)x, (float)y);}
	public static Vec3f vec3f(double x, double y, double z)				{	return new Vec3f((float)x, (float)y, (float)z);}
	public static Vec4f vec4f(double x, double y, double z, double w)	{	return new Vec4f((float)x, (float)y, (float)z, (float)w);}

	
	public static Vec1f vec1f(float x)									{	return new Vec1f(x);}
	public static Vec2f vec2f(float x, float y)							{	return new Vec2f(x, y);}
	public static Vec3f vec3f(float x, float y, float z)				{	return new Vec3f(x, y, z);}
	public static Vec4f vec4f(float x, float y, float z, float w)		{	return new Vec4f(x, y, z, w);}
	
	public static Vec1d vec1d(double x)									{	return new Vec1d(x);}
	public static Vec2d vec2d(double x, double y)						{	return new Vec2d(x, y);}
	public static Vec3d vec3d(double x, double y, double z)				{	return new Vec3d(x, y, z);}
	public static Vec4d vec4d(double x, double y, double z, double w)	{	return new Vec4d(x, y, z, w);}	
	
	public static Vec1i vec1i(int x)									{	return new Vec1i(x);}
	public static Vec2i vec2i(int x, int y)								{	return new Vec2i(x, y);}
	public static Vec3i vec3i(int x, int y, int z)						{	return new Vec3i(x, y, z);}
	public static Vec4i vec4i(int x, int y, int z, int w)				{	return new Vec4i(x, y, z, w);}
	
}
