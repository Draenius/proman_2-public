package proman.math.vector;

public interface Vec4<T> extends Vec3<T>
{
	public abstract T getW();
	public abstract void setW(T w);
	
	public abstract Vec4<T> add(Vec4<?> add);
	public abstract Vec4<T> sub(Vec4<?> sub);
	public abstract Vec4<T> mul(Vec4<?> mul);
	public abstract Vec4<T> div(Vec4<?> div);
	
	public abstract Vec4<T> clone();
	public abstract int hashCode();
	public abstract boolean equals(Object o);
}
