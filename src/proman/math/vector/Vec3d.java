package proman.math.vector;

public class Vec3d extends Vec2d implements Vec3<Double>
{
	public double z;
	
	public Vec3d(double x, double y, double z) {
		super(x, y);
		this.z = z;
	}


	@Override
	public Double getZ() {
		return this.z;
	}

	@Override
	public void setZ(Double z) {
		this.z = z;
	}

	@Override
	public Vec3d add(Vec3<?> add) {
		if(compatibleType(add.getX()))
			return new Vec3d(
					this.x + doubleValue(add.getX()),
					this.y + doubleValue(add.getY()),
					this.z + doubleValue(add.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3d sub(Vec3<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec3d(
					this.x - doubleValue(sub.getX()),
					this.y - doubleValue(sub.getY()),
					this.z - doubleValue(sub.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3d mul(Vec3<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec3d(
					this.x * doubleValue(mul.getX()),
					this.y * doubleValue(mul.getY()),
					this.z * doubleValue(mul.getZ()));
		else
			return this.clone();
	}
	
	@Override
	public Vec3d div(Vec3<?> div) {
		if(compatibleType(div.getX()))
			return new Vec3d(
					this.x / doubleValue(div.getX()),
					this.y / doubleValue(div.getY()),
					this.z / doubleValue(div.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3d clone() {
		return new Vec3d(this.x, this.y, this.z);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec3<?>)
			return compatibleType(( (Vec3<?>)o ).getX()) ? ( (Vec3<?>)o ).getX().equals(this.getX()) && ( (Vec3<?>)o ).getY().equals(this.getY()) && ( (Vec3<?>)o ).getZ().equals(this.getZ()): false;
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.x + (int)this.y + (int)this.z;
	}

	@Override
	public String toString() {
		return this.x+"|"+this.y+"|"+this.z;
	}


}
