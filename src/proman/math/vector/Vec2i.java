package proman.math.vector;

public class Vec2i extends Vec1i implements Vec2<Integer>
{
	public int y;
	
	public Vec2i(int x, int y) {
		super(x);
		this.y = y;
	}


	@Override
	public Integer getY() {
		return this.y;
	}

	@Override
	public void setY(Integer y) {
		this.y = y;
	}

	@Override
	public Vec2i add(Vec2<?> add) {
		if(compatibleType(add.getX()))
			return new Vec2i(
					this.x + intValue(add.getX()),
					this.y + intValue(add.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2i sub(Vec2<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec2i(
					this.x - intValue(sub.getX()),
					this.y - intValue(sub.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2i mul(Vec2<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec2i(
					this.x * intValue(mul.getX()),
					this.y * intValue(mul.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2i div(Vec2<?> div) {
		if(compatibleType(div.getX()))
			return new Vec2i(
					this.x / intValue(div.getX()),
					this.y / intValue(div.getY()));
		else
			return this.clone();
	}

	@Override
	public Vec2i clone() {
		return new Vec2i(this.x, this.y);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec2<?>)
			return compatibleType(( (Vec2<?>)o ).getX()) ? ( (Vec2<?>)o ).getX().equals(this.getX()) && ( (Vec2<?>)o ).getY().equals(this.getY()) : false;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		String code = ""+(this.x>=0?"1":"2")+(this.y>=0?"1":"2")+(this.x>0? x:-x)+(this.y>0?y:-y);
		if(code.length()>9) code = code.substring(0, 8);
		return (code.startsWith("-") ? -1 : 1) * Integer.valueOf(code.replaceAll("-", ""));
	}
	
	@Override
	public String toString() {
		return this.x+"|"+this.y;
	}
}
