package proman.math.vector;

public interface Vec3<T> extends Vec2<T>
{
	public abstract T getZ();
	public abstract void setZ(T z);
	
	public abstract Vec3<T> add(Vec3<?> add);
	public abstract Vec3<T> sub(Vec3<?> sub);
	public abstract Vec3<T> mul(Vec3<?> mul);
	public abstract Vec3<T> div(Vec3<?> div);
	
	public abstract Vec3<T> clone();
	public abstract int hashCode();
	public abstract boolean equals(Object o);
}
