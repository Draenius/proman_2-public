package proman.math.vector;

public interface Vec2<T> extends Vec1<T> 
{
	public abstract T getY();
	public abstract void setY(T y);
	
	public abstract Vec2<T> add(Vec2<?> add);
	public abstract Vec2<T> sub(Vec2<?> sub);
	public abstract Vec2<T> mul(Vec2<?> mul);
	public abstract Vec2<T> div(Vec2<?> div);
	
	public abstract Vec2<T> clone();
	public abstract int hashCode();
	public abstract boolean equals(Object o);
}
