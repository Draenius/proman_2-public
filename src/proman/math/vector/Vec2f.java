package proman.math.vector;

public class Vec2f extends Vec1f implements Vec2<Float>
{
	public float y;
	
	public Vec2f(float x, float y) {
		super(x);
		this.y = y;
	}


	@Override
	public Float getY() {
		return this.y;
	}

	@Override
	public void setY(Float y) {
		this.y = y;
	}

	
	public Vec2f add(Vec2<?> add) {
		return new Vec2f(
				this.x + floatValue(add.getX()),
				this.y + floatValue(add.getY()));
	}

	@Override
	public Vec2f sub(Vec2<?> sub) {
		return new Vec2f(
				this.x - floatValue(sub.getX()),
				this.y - floatValue(sub.getY()));
	}

	@Override
	public Vec2f mul(Vec2<?> mul) {
		return new Vec2f(
				this.x * floatValue(mul.getX()),
				this.y * floatValue(mul.getY()));
	}

	@Override
	public Vec2f div(Vec2<?> div) {
		return new Vec2f(
				this.x / floatValue(div.getX()),
				this.y / floatValue(div.getY()));
	}

	@Override
	public Vec2f clone() {
		return new Vec2f(this.x, this.y);
	}
	
	public Vec2d toDouble() {
		return new Vec2d(this.x, this.y);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec2<?>)
			return compatibleType(( (Vec2<?>)o ).getX()) ? ( (Vec2<?>)o ).getX().equals(this.getX()) && ( (Vec2<?>)o ).getY().equals(this.getY()) : false;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.x+ (int)this.y;
	}
	
	@Override
	public String toString() {
		return this.x+"|"+this.y;
	}
}
