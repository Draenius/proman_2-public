package proman.math.vector;

public class Vec1d implements Vec1<Double>
{
	public double x;
	
	@Override
	public Double getX() 		{	return this.x;}
	@Override
	public void setX(Double x) 	{	this.x = x;}
	
	public Vec1d(double x)
	{
		this.x = x;
	}
	
	@Override
	public Vec1d add(Vec1<?> add) {
		if(compatibleType(add.getX()))
			return new Vec1d(
					this.x + doubleValue(add.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1d sub(Vec1<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec1d(
					this.x - doubleValue(sub.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1d mul(Vec1<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec1d(
					this.x * doubleValue(mul.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1d div(Vec1<?> div) {
		if(compatibleType(div.getX()))
			return new Vec1d(
					this.x / doubleValue(div.getX()));
		else
			return this.clone();
	}

	@Override
	public Vec1d clone() {
		return new Vec1d(this.x);
	}

	public boolean equals(Object o) {
		if(o instanceof Vec1<?>)
			return compatibleType(( (Vec1<?>)o ).getX()) ? ( (Vec1<?>)o ).getX().equals(this.getX()) : false;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.x;
	}
	
	@Override
	public String toString() {
		return this.x+"";
	}
	
	public static double doubleValue(Object o)
	{
			 if(o instanceof Byte)		return ((Byte)o).doubleValue();
		else if(o instanceof Short)		return ((Short)o).doubleValue();
		else if(o instanceof Integer)	return ((Integer)o).doubleValue();
		else if(o instanceof Float)		return ((Float)o).doubleValue();
		else if(o instanceof Long)		return ((Long)o).doubleValue();
		else if(o instanceof Double)	return ((Double)o).doubleValue();
		else return Double.NaN;
	}
	
	public static boolean compatibleType(Object o)
	{
		return o instanceof Byte || o instanceof Short || o instanceof Integer || o instanceof Float || o instanceof Long || o instanceof Double;
	}
	
}
