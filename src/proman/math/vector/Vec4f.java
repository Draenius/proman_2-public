package proman.math.vector;

public class Vec4f extends Vec3f implements Vec4<Float>
{
	public float w;
	
	
	public Vec4f(Vec2f xy, Vec2f zw)
	{
		this(xy.x, xy.y, zw.x, zw.y);
	}
	
	
	public Vec4f(float x, float y, float z, float w) {
		super(x, y, z);
		this.w = w;
	}


	@Override
	public Float getW() {
		return this.w;
	}

	@Override
	public void setW(Float w) {
		this.w = w;
	}

	@Override
	public Vec4f add(Vec4<?> add) {
		if(compatibleType(add.getX()))
			return new Vec4f(
					this.x + floatValue(add.getX()),
					this.y + floatValue(add.getY()),
					this.z + floatValue(add.getZ()),
					this.w + floatValue(add.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4f sub(Vec4<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec4f(
					this.x - floatValue(sub.getX()),
					this.y - floatValue(sub.getY()),
					this.z - floatValue(sub.getZ()),
					this.w - floatValue(sub.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4f mul(Vec4<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec4f(
					this.x * floatValue(mul.getX()),
					this.y * floatValue(mul.getY()),
					this.z * floatValue(mul.getZ()),
					this.w * floatValue(mul.getW()));
		else
			return this.clone();
	}
	
	@Override
	public Vec4f div(Vec4<?> div) {
		if(compatibleType(div.getX()))
			return new Vec4f(
					this.x / floatValue(div.getX()),
					this.y / floatValue(div.getY()),
					this.z / floatValue(div.getZ()),
					this.w / floatValue(div.getW()));
		else
			return this.clone();
	}

	@Override
	public Vec4f clone() {
		return new Vec4f(this.x, this.y, this.z, this.w);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec4<?>)
			return compatibleType(( (Vec4<?>)o ).getX()) ? ( (Vec4<?>)o ).getX().equals(this.getX()) && ( (Vec4<?>)o ).getY().equals(this.getY()) && ( (Vec4<?>)o ).getZ().equals(this.getZ())  && ( (Vec4<?>)o ).getW().equals(this.getW()): false;
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.x + (int)this.y + (int)this.z + (int)this.w;
	}
	
	@Override
	public String toString() {
		return this.x+"|"+this.y+"|"+this.z+"|"+this.w;
	}
}
