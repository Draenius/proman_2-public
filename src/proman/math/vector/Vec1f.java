package proman.math.vector;

public class Vec1f implements Vec1<Float>
{
	public float x;
	
	@Override
	public Float getX() 		{	return this.x;}
	@Override
	public void setX(Float x) 	{	this.x = x;}
	
	public Vec1f(float x)
	{
		this.x = x;
	}
	
		
	
	@Override
	public Vec1f add(Vec1<?> add) {
		return new Vec1f(
			this.x + floatValue(add.getX()));
	}

	@Override
	public Vec1f sub(Vec1<?> sub) {
		return new Vec1f(
			this.x - floatValue(sub.getX()));
	}

	@Override
	public Vec1f mul(Vec1<?> mul) {
		return new Vec1f(
			this.x * floatValue(mul.getX()));
	}

	@Override
	public Vec1f div(Vec1<?> div) {
		return new Vec1f(
			this.x / floatValue(div.getX()));
	}

	@Override
	public Vec1f clone() {
		return new Vec1f(this.x);
	}
	
	public Vec1d toDouble() {
		return new Vec1d(this.x);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec1<?>)
			return compatibleType(( (Vec1<?>)o ).getX()) ? ( (Vec1<?>)o ).getX().equals(this.getX()) : false;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return (int)this.x;
	}
	
	@Override
	public String toString() {
		return this.x+"";
	}
	
	public static float floatValue(Object o)
	{
			 if(o instanceof Float)		return (Float)o;
		else if(o instanceof Double)	return ((Double)o).floatValue();
		else if(o instanceof Integer)	return ((Integer)o).floatValue();
		else if(o instanceof Short)		return ((Short)o).floatValue();
		else if(o instanceof Byte)		return ((Byte)o).floatValue();
		else if(o instanceof Long)		return ((Long)o).floatValue();
		else return Float.NaN;
	}
	
	public static boolean compatibleType(Object o)
	{
		return o instanceof Float || o instanceof Double || o instanceof Byte || o instanceof Short || o instanceof Integer || o instanceof Long;
	}
}
