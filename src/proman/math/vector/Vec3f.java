package proman.math.vector;

public class Vec3f extends Vec2f implements Vec3<Float>
{
	public float z;
	
	public Vec3f(float x, float y, float z) {
		super(x, y);
		this.z = z;
	}


	@Override
	public Float getZ() {
		return this.z;
	}

	@Override
	public void setZ(Float z) {
		this.z = z;
	}

	@Override
	public Vec3f add(Vec3<?> add) {
		if(compatibleType(add.getX()))
			return new Vec3f(
					this.x + floatValue(add.getX()),
					this.y + floatValue(add.getY()),
					this.z + floatValue(add.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3f sub(Vec3<?> sub) {
		if(compatibleType(sub.getX()))
			return new Vec3f(
					this.x - floatValue(sub.getX()),
					this.y - floatValue(sub.getY()),
					this.z - floatValue(sub.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3f mul(Vec3<?> mul) {
		if(compatibleType(mul.getX()))
			return new Vec3f(
					this.x * floatValue(mul.getX()),
					this.y * floatValue(mul.getY()),
					this.z * floatValue(mul.getZ()));
		else
			return this.clone();
	}
	
	@Override
	public Vec3f div(Vec3<?> div) {
		if(compatibleType(div.getX()))
			return new Vec3f(
					this.x / floatValue(div.getX()),
					this.y / floatValue(div.getY()),
					this.z / floatValue(div.getZ()));
		else
			return this.clone();
	}

	@Override
	public Vec3f clone() {
		return new Vec3f(this.x, this.y, this.z);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Vec3<?>)
			return compatibleType(( (Vec3<?>)o ).getX()) ? ( (Vec3<?>)o ).getX().equals(this.getX()) && ( (Vec3<?>)o ).getY().equals(this.getY()) && ( (Vec3<?>)o ).getZ().equals(this.getZ()): false;
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.x + (int)this.y + (int)this.z;
	}
	
	@Override
	public String toString() {
		return this.x+"|"+this.y+"|"+this.z;
	}


}
