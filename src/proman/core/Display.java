package proman.core;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowMaximizeCallbackI;
import org.lwjgl.opengl.GL;

import proman.input.Keyboard;
import proman.input.Mouse;
import proman.math.vector.Vec2i;
import proman.util.io.Address;

public class Display 
{
	private static boolean changeFullscreen = false;
	private static boolean fullscreen = false;
	
	private static String title = "";
	private static Address icon;
	private static Vec2i size;
	private static Vec2i pos = new Vec2i(0, 0);
	private static boolean maximized;
	private static boolean resizable;
	
	private static float gamma = 1;
	
	private static Vec2i dim = new Vec2i(0, 0);
	private static boolean resized = true;
	
	private static boolean created = false;
	
	private static long currentWindow;
	
	private static IntBuffer xBuffer = BufferUtils.createIntBuffer(1);
	private static IntBuffer yBuffer = BufferUtils.createIntBuffer(1);
	
	public static void update()
	{
		if(!Core.alive)
		{
			Callbacks.glfwFreeCallbacks(currentWindow);
			GLFW.glfwDestroyWindow(currentWindow);
			GLFW.glfwTerminate();
			GLFW.glfwSetErrorCallback(null).free();
			System.exit(0);
			return;
		}
		
		if(!created)
			System.exit(0);
		
		if(changeFullscreen)
		{
			applyFullscreen(fullscreen);
			changeFullscreen = false;
		}
		
		GLFW.glfwSwapBuffers(currentWindow);
		//TODO GLFW.glfwPollEvents();
				
		glLoadIdentity();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		
		GLFW.glfwPollEvents();
		//Audio.update();
		
		Vec2i size = getSize();
		if(size.x != dim.x || size.y != dim.y)
		{
			resized = true;
			dim.x = size.x;
			dim.y = size.y;
		}
		else
			resized = false;
	}
	
	
	public static void initiate(int width, int height) throws Exception
	{
		GLFWErrorCallback.createPrint(System.err).set();
		if(!GLFW.glfwInit())
			throw new ManagerException("unable to initialize GLFW");
		
		GLFW.glfwDefaultWindowHints();
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE);
		GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, 8);
		GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_ANY_PROFILE);
		
		currentWindow = GLFW.glfwCreateWindow(width, height, "", 0, 0);
		if(currentWindow == 0)
			throw new ManagerException("Unable to create GLFW window");
		
		//MemoryStack.stackPush();
		GLFW.glfwMakeContextCurrent(currentWindow);
		GLFW.glfwSwapInterval(0);
		
		GLFW.glfwShowWindow(currentWindow);
		
		GLFW.glfwSetWindowMaximizeCallback(currentWindow, new GLFWWindowMaximizeCallbackI()
		{
			@Override public void invoke(long window, boolean maximized)
			{
				if(window == currentWindow)
					Display.maximized = maximized;
			}
		});

		
		GL.createCapabilities();
		setupGL();
		
		dim = new Vec2i(width, height);
		size = new Vec2i(width, height);
		
		created = true;
	}
	
	private static void setupGL()
	{
		/*try
		{
			org.lwjgl.opengl.Display.create();
			//org.lwjgl.opengl.Display.setVSyncEnabled(true);
		}
		catch(Exception e)
		{
			throw new ManagerException("Unable to initiate OpenGL: \n"+e.getLocalizedMessage());
		}*/
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0f, 1f, 0f, 1f, -1f, 1f);
		glMatrixMode(GL_MODELVIEW);
		//glClearColor(1, 1, 1, 1);
		
		glShadeModel(GL_SMOOTH);
		
		glDisable(GL_DEPTH_TEST);
		
		glEnable(GL_TEXTURE_2D);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);	
	}
	
	
	
	/**  Tries to set the image file "adress" as icon image*/
	public static void setIcon(Address address)
	{
		Vec2i imageSize = new Vec2i(0, 0);
		ByteBuffer iconData = loadIconData(address, imageSize);
		
		GLFWImage.Buffer glfwImages = GLFWImage.calloc(1);
		GLFWImage glfwImage = glfwImages.get(0);
		glfwImage.width(imageSize.x);
		glfwImage.height(imageSize.y);
		
		glfwImage.pixels(iconData);
		GLFW.glfwSetWindowIcon(currentWindow, glfwImages);
		glfwImages.free();
		
		Display.icon = address;
	}
	
	/** Tries to set "title" as frame title*/
	public static void setTitle(String title)
	{
		GLFW.glfwSetWindowTitle(currentWindow, title);
		Display.title = title;
	}
	
	private static final String NOT_INIT = "Core is not initiated. Call Core.initiate() first.";
	
	/** Tries to set the frame size to w*h */
	public static void setFrameSize(int w, int h)// throws ManagerException
	{
		GLFW.glfwSetWindowSize(currentWindow, w, h);
		size = new Vec2i(w, h);
	}
	
	/** Toggles frame resizability to "resizable" */
	public static void setResizable(boolean resizable) throws ManagerException
	{
		GLFW.glfwSetWindowAttrib(currentWindow, GLFW.GLFW_RESIZABLE, 
				resizable ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
		Display.resizable = resizable;
	}
	
	/** Sets frame size to maximized or default */
	public static void setMaximized(boolean maximized) throws ManagerException
	{
		/*GLFW.glfwSetWindowAttrib(currentWindow, GLFW.GLFW_MAXIMIZED, 
				maximized ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);*/
		if(maximized)
			GLFW.glfwMaximizeWindow(currentWindow);
		Display.maximized = maximized;
	}
	
	/** Enables/disables usage of vSynch */
	public static void useVSynch(boolean vSynch) throws ManagerException
	{	try{}//org.lwjgl.opengl.Display.setVSyncEnabled(vSynch);}
		catch(NullPointerException e){ throw new ManagerException(NOT_INIT);}
	}
	
	//** Returns whether the OpenGL context has the current focus*/
	/*public static boolean hasFocus()
	{
		return jFrame.hasFocus();
	}*/
	
	
	/** Returns whether or not the display frame was resized since last frame*/
	public static boolean wasResized()
	{
		return resized;
	}
	
	/** Returns the display's current dimensions. NOT the display's frame's dimensions!*/
	public static Vec2i getSize()
	{
		GLFW.glfwGetWindowSize(currentWindow, xBuffer, yBuffer);
		return getBufferedCoord();//glSurface.getWidth(), glSurface.getHeight());
	}
	
	public static void setFullscreen(boolean fullscreen)
	{
		if(fullscreen == Display.fullscreen) return;
		Display.changeFullscreen = true;
		Display.fullscreen = fullscreen;
	}
	
	private static void applyFullscreen(boolean fullscreen)
	{
		long monitor = Display.getCurrentMonitor();
		
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, Display.resizable ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
		GLFW.glfwWindowHint(GLFW.GLFW_FOCUSED, GLFW.GLFW_TRUE);
		GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, 8);
		
		GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_ANY_PROFILE);
		
		GLFW.glfwWindowHint(GLFW.GLFW_MAXIMIZED, fullscreen ? GLFW.GLFW_FALSE : 
			Display.maximized ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
		GLFW.glfwWindowHint(GLFW.GLFW_DECORATED, !fullscreen ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
		
		
		//GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_COMPAT_PROFILE);
		
		if(fullscreen)
		{
			GLFW.glfwGetWindowPos(currentWindow, xBuffer, yBuffer);
			Display.pos = getBufferedCoord();
		}
		
		long oldWindow = currentWindow;		
		
		currentWindow = GLFW.glfwCreateWindow(Display.size.x, Display.size.y, "", 0, currentWindow);
		
		if(fullscreen)
		{
			GLFW.glfwGetMonitorPos(monitor, xBuffer, yBuffer);
			Vec2i pos = getBufferedCoord();
			GLFWVidMode vidMode = GLFW.glfwGetVideoMode(monitor);
			
			GLFW.glfwSetWindowSize(currentWindow, vidMode.width(), vidMode.height());
			GLFW.glfwSetWindowPos(currentWindow, pos.x, pos.y);
		}
		else
		{
			GLFW.glfwSetWindowPos(currentWindow, pos.x, pos.y);
		}
		
		Display.setIcon(Display.icon);
		Display.setTitle(Display.title);
		
		//MemoryStack.stackPush();
		//GLFW.glfwGetCurrentContext();
		
		GLFW.glfwSetGamma(monitor, Display.gamma);
		
		GLFW.glfwMakeContextCurrent(currentWindow);
		GLFW.glfwSwapInterval(0);
		GLFW.glfwShowWindow(currentWindow);
		
		GLFW.glfwDestroyWindow(oldWindow);
		
		GLFW.glfwSetWindowMaximizeCallback(currentWindow, new GLFWWindowMaximizeCallbackI()
		{
			@Override public void invoke(long window, boolean maximized)
			{
				if(window == currentWindow)
					Display.maximized = maximized;
			}
		});
		
		Keyboard.create();
		Mouse.create();
		
		GL.createCapabilities();
		setupGL();

		
		Display.fullscreen = fullscreen;
	}
	
	
	
	
	/*private static JFrame defaultContentFrame(int width, int height, boolean fullscreen)
	{
		JFrame frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		frame.setSize(width, height);
		frame.setBackground(Color.BLACK);
		frame.setTitle(title);
		setIcon(icon);
		frame.setUndecorated(fullscreen);
		
		return frame;
	}*/
	
	public static long getGlfwWindowHandle()
	{
		return currentWindow;
	}
	
	
	private static ByteBuffer loadIconData(Address address, Vec2i size)
	{
		/*try
		{
			ByteBuffer fileBuffer = IOUtil.readIntoBuffer(address);
			IntBuffer comp = BufferUtils.createIntBuffer(1);
			if(!STBImage.stbi_info_from_memory(fileBuffer, xBuffer, yBuffer, comp))
				throw new ManagerException("Could not load image file: "+STBImage.stbi_failure_reason());

			
			ByteBuffer imageBuffer = STBImage.stbi_load_from_memory(fileBuffer, xBuffer, yBuffer, comp, 0);
			if(imageBuffer == null)
				throw new ManagerException("Could not load image file: "+STBImage.stbi_failure_reason());
			
			return imageBuffer;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return BufferUtils.createByteBuffer(0);*/
		
		BufferedImage image;
		try
		{
			InputStream input = address.openInputStream();
			image = ImageIO.read(input);
			input.close();
		} 
		catch(Exception e)
		{
			System.err.println("Could not load window icon: "+e.getLocalizedMessage());
			return null;
		}
		if(image == null)
			return null;
		
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);

		for(int y = 0; y < image.getHeight(); y++)
		{
			for(int x = 0; x < image.getWidth(); x++)
			{
				int pixel = pixels[y * image.getWidth() + x];
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) (pixel & 0xFF));
				buffer.put((byte) ((pixel >> 24) & 0xFF));
			}
		}

		buffer.flip();
		
		size.x = image.getWidth();
		size.y = image.getHeight();
		return buffer;
		
	}
	
	
	public static long getCurrentMonitor()
	{
		PointerBuffer monitorPointerBuffer = GLFW.glfwGetMonitors();
		
		Vec2i dim = Display.getSize();
		GLFW.glfwGetWindowPos(currentWindow, xBuffer, yBuffer);
		Vec2i pos = new Vec2i(xBuffer.get(0), yBuffer.get(0));
		
		long fallbackMonitor = 0;
		
		for(int i=0;i<monitorPointerBuffer.capacity();i++)
		{
			long monitor = monitorPointerBuffer.get(i);
			
			if(fallbackMonitor == 0)
				fallbackMonitor = monitor;
			
			GLFW.glfwGetMonitorPos(monitor, xBuffer, yBuffer);
			int x = xBuffer.get(0), y = yBuffer.get(0);
			GLFWVidMode mode = GLFW.glfwGetVideoMode(monitor);
			int w = mode.width(), h = mode.height();
			
			if((pos.x+dim.x*.5f) > x && (pos.x+dim.x*.5f) < x+w)
				if(pos.y > y && pos.y < y+h)
					return monitor;
		}
		return fallbackMonitor;
	}
	
	private static Vec2i getBufferedCoord()
	{
		return new Vec2i(xBuffer.get(0), yBuffer.get(0));
	}
	
	public static void setDisplayGamma(float gamma)
	{
		GLFW.glfwSetGamma(getCurrentMonitor(), gamma);
	}
	
	public static float getDisplayGamma()
	{
		return Display.gamma;
	}
}
