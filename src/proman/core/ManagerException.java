package proman.core;

@SuppressWarnings("serial")
public class ManagerException extends Exception
{
	private String message;
	
	public ManagerException(String message)
	{
		this.message = message;
	}
	
	public String getLocalizedMessage()
	{
		return this.message;
	}
}
