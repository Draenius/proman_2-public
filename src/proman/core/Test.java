package proman.core;

import proman.input.Key;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.rendering.Immediate2DRenderer;
import proman.rendering.Screen;
import proman.time.DeltaFrameTimer;
import proman.util.Color;

public class Test extends Immediate2DRenderer
{
	public static void main(String[] args) throws Exception
	{
		Core.initiate(new LaunchConfiguration()
		{
			@Override public void launch() throws ManagerException
			{
				this.display.size = new Vec2i(800, 600);
				//this.fontset.addFont(new Address("res://simplefont.png"), false, "font");
			}
		});
		
		Core.setCurrentScreen(new Screen(new Vec2f(16, 9)));
		Core.currentScreen().setRenderer(new Test());
		
		while(true)
			Core.update();
	}
	
	float x = 8f;
	float vel = 0f;
	DeltaFrameTimer frameTimer = new DeltaFrameTimer();
	
	@Override
	public void update()
	{
		int frames = this.frameTimer.getNewFrames(1.0/90.0);
		for(int i=0;i<frames;i++)
		{
			float dir = (Key.A.isDown() ? -1 : 0) + (Key.D.isDown() ? 1 : 0);
			vel = (40f*vel+dir*.2f)/41f;
			x += vel;
		}
	}
	
	@Override
	public void render()
	{
		
		this.drawQuad(new Vec4f(this.x-.05f, 4.5f-.05f, .1f, .1f), Color.WHITE);
	}
}
