package proman.core;

import org.lwjgl.opengl.GL11;

import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.rendering.Renderer;
import proman.rendering.Immediate2DRenderer;
import proman.rendering.Screen;
import proman.shader.GL2ShaderProgram;
import proman.texture.FrameBufferObject;
import proman.texture.Texture;
import proman.util.Color;

public class BufferingScreen extends Screen
{
	Immediate2DRenderer utilContent = new Immediate2DRenderer(){public void update(){} public void render(){}};
	public FrameBufferObject frameBuffer;
	GL2ShaderProgram shader;
	
	boolean shaderUsable;
	boolean bufferUsable;
	
	public boolean buffering;
	public float brightness = 1;
	
	public boolean antiAlias = true;
	
	private boolean applied;
	
	@Override
	public void setAsCurrent()
	{
		Core.setCurrentScreen(this);
		this.applied = true;
	}
	
	public BufferingScreen(Vec2f aspect) throws ManagerException
	{
		super(aspect);
		try
		{
			this.frameBuffer = new FrameBufferObject(new Vec2i(1, 1));
			this.bufferUsable = true;
		}
		catch(Exception e)
		{
			this.bufferUsable = false;
			this.shaderUsable = false;
			e.printStackTrace();
		}
	}
	
	@Override
	public void render() throws ManagerException
	{
		if(Display.wasResized() || this.applied || this.bufferUsable)
		{
			this.applied = false;
			this.applyViewportForResolution(Display.getSize());
		}
		
		this.renderFramebuffer();
		
		Renderer content = this.getCurrentRenderer();
		if(content != null)
		{
			if(this.bufferUsable)
			{
				this.frameBuffer.bindToRender(this);
				content.render();
				
				FrameBufferObject.releaseAny();
			}
			else
				content.render();

		}
	}
	
	private void renderFramebuffer() throws ManagerException
	{
		if(this.bufferUsable)
		{
			Vec2f vec = this.antiAlias ? new Vec2f(2, 2) : new Vec2f(1, 1);
			Vec2i bufferRes = new Vec2i(1,1).mul(vec.mul(this.getDisplaySize()));
			
			if(	this.frameBuffer == null ||
				!bufferRes.equals(this.frameBuffer.dimensions()))
			{
				if(this.frameBuffer != null)
					this.frameBuffer.dispose();
				this.frameBuffer = new FrameBufferObject(bufferRes);
			}
			GL11.glDisable(GL11.GL_BLEND);
			this.utilContent.drawQuad(
				this.getVisibleArea(Display.getSize()), 
				Color.WHITE, 
				new Texture(this.frameBuffer, true));
			GL11.glEnable(GL11.GL_BLEND);

		}
	}
		
	public boolean bufferUsable()	{	return this.bufferUsable;}
	public boolean shaderUsable()	{	return this.shaderUsable;}
	
}
