package proman.core;

import java.util.ArrayList;

import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.texture.Texture;
import proman.util.io.Address;
import proman.audio.Sound;
import proman.font.Font;

public abstract class LaunchConfiguration
{
	public Display display;
	public Textureset textureset;
	public Soundset soundset;
	public Fontset fontset;
		
	public LaunchConfiguration() throws ManagerException
	{
		this.display = new Display();
		this.soundset = new Soundset();
		this.textureset = new Textureset();
		this.fontset = new Fontset();
		
		launch();
	}
	
	public abstract void launch() throws ManagerException;
	
	
	void apply() throws Exception
	{
		if(this.display.size != null)
			proman.core.Display.initiate(this.display.size.x, this.display.size.y);
		else
			proman.core.Display.initiate(0, 0);
		
		if(this.display.title != null)
			proman.core.Display.setTitle(this.display.title);
		
		if(this.display.icon != null)
			proman.core.Display.setIcon(this.display.icon);
		
		proman.core.Display.setMaximized(this.display.maximized);
		
		proman.core.Display.setResizable(this.display.resizable);
		
		
		for(int i=0;i<this.fontset.ids.size();i++)
			proman.font.Fontset.addFont(new Font(this.fontset.sources.get(i), this.fontset.smoothed.get(i)), this.fontset.ids.get(i));
		
		for(int i=0;i<this.soundset.ids.size();i++)
			proman.audio.Soundset.addSound(new Sound(this.soundset.sources.get(i)), this.soundset.ids.get(i));
		
		ArrayList<proman.texture.Sampler2D> samplers = new ArrayList<proman.texture.Sampler2D>();
		for(Sampler sampler : this.textureset.samplers)
			samplers.add(new proman.texture.Sampler2D(sampler.adress));
		
		for(int i=0;i<this.textureset.texIds.size();i++)
			proman.texture.Textureset.addTexture(
					new Texture(samplers.get(this.textureset.texSources.get(i)), this.textureset.texCoords.get(i), this.textureset.texAntiAliasing.get(i)), 
					this.textureset.texIds.get(i));
	}
	
	
	
	
	
	public class Soundset
	{
		private ArrayList<Address> sources;
		private ArrayList<String> ids;
		
		public Soundset()
		{
			this.sources = new ArrayList<Address>();
			this.ids = new ArrayList<String>();
		}
		
		public void loadSound(Address adress, String id)
		{
			this.ids.add(id);
			this.sources.add(adress);
		}
	}
	
	
	public class Fontset
	{
		private ArrayList<Address> sources;
		private ArrayList<Boolean> smoothed;
		private ArrayList<String> ids;
		
		public Fontset()
		{
			this.sources = new ArrayList<Address>();
			this.smoothed = new ArrayList<Boolean>();
			this.ids = new ArrayList<String>();
		}
		
		public void addFont(Address source, boolean smoothed, String id)
		{
			this.ids.add(id);
			this.smoothed.add(smoothed);
			this.sources.add(source);
		}
	}
	
	
	public class Textureset
	{
		private ArrayList<Sampler> samplers;
		private ArrayList<Boolean> texAntiAliasing;
		private ArrayList<Vec2f[]> texCoords;
		private ArrayList<Integer> texSources;
		private ArrayList<String> texIds;
		
		
		public Textureset()
		{
			this.samplers = new ArrayList<LaunchConfiguration.Sampler>();
			this.texAntiAliasing = new ArrayList<Boolean>();
			this.texCoords = new ArrayList<Vec2f[]>();
			this.texSources = new ArrayList<Integer>();
			this.texIds = new ArrayList<String>();
		}
		
		public Sampler loadSampler(Address adress)
		{
			Sampler sampler = new Sampler(adress);
			sampler.id = this.samplers.size();
			this.samplers.add(sampler);
			return sampler;
		}
		
		
		
		
		
		public void addTexture(Sampler sampler, Vec2f[] texCoords, boolean antiAliasing, String id)
		{
			this.texSources.add(sampler.id);
			this.texAntiAliasing.add(antiAliasing);
			this.texIds.add(id);
			
			this.texCoords.add(texCoords);
		}
		
		public void addTexture(Sampler sampler, float[][] texCoords, boolean antiAliasing, String id)
		{
			Vec2f[] vecs = new Vec2f[texCoords.length];
			for(int i=0;i<texCoords.length;i++)
				vecs[i] = new Vec2f(texCoords[i][0], texCoords[i][1]);
			
			addTexture(sampler, vecs, antiAliasing, id);
		}
		
		public void addTexture(Sampler sampler, Vec4f excerpt, boolean antiAliasing, String id)
		{
			this.addTexture(sampler, new Vec2f[] {
					new Vec2f(excerpt.x, 			excerpt.y),
					new Vec2f(excerpt.x+excerpt.z, 	excerpt.y),
					new Vec2f(excerpt.x+excerpt.z, 	excerpt.y+excerpt.w),
					new Vec2f(excerpt.x, 			excerpt.y+excerpt.w)
				}, antiAliasing, id);
		}
		
		public void addTexture(Sampler sampler, boolean antiAliasing, String id)
		{
			this.addTexture(sampler, new Vec2f[] {
					new Vec2f(0, 0), new Vec2f(1, 0), new Vec2f(1, 1), new Vec2f(0, 1)
				}, antiAliasing, id);
		}
	}
	
	
	public class Sampler
	{
		Address adress;
		Integer id;
		Sampler(Address adress)
		{
			this.adress = adress;
		}
	}
	
	
	public class Display
	{
		public Vec2i size;
		public String title;
		public Address icon;
		public boolean resizable;
		public boolean maximized;
		
		public Display()
		{
			this.resizable = true;
			this.maximized = false;
		}
	}
}
