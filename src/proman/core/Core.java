package proman.core;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import proman.audio.Audio;
import proman.input.Keyboard;
import proman.input.Mouse;
import proman.rendering.Screen;

public class Core
{
	public static final double VERSION = 2.01;
	static boolean alive;
	
	private static Screen currentScreen;
	
	
	public static void setCurrentScreen(Screen screen)
	{
		currentScreen = screen;
	}
	
	public static Screen currentScreen()
	{
		return currentScreen;
	}
	
	public static void update() throws Exception
	{
		if(GLFW.glfwWindowShouldClose(Display.getGlfwWindowHandle()))
			destroy();
		
		try
		{
			Keyboard.update();
			Mouse.update();
			Display.update();
			//Audio.update();
			
			if(currentScreen != null)
			{
				currentScreen.update();
				currentScreen.render();
			}
		}
		catch(ManagerException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	/** Updates classes KeyboardListener and MouseListener*/
	public static void updateInputs()
	{
		Keyboard.update();
		Mouse.update();
	}
	
	/** Initiates and prepares the pRoman usage and opens the main screen*/
	public static void initiate(LaunchConfiguration config) throws Exception
	{
		alive = true;
		
		Runtime.getRuntime().addShutdownHook(new Thread() 
		{
			public void run()
			{
				try
				{
					Audio.destroy();
				}
				catch(Exception e){}
				Runtime.getRuntime().halt(0);
			}
		});
		
		Audio.create();
		
		if(config != null)
			config.apply();
		else
			Display.initiate(0, 0);
	}
	
	
	/** Exits the current program*/
	public static void destroy()
	{
		alive = false;
	}
	
	
	
		
	/** Returns the GL context's GPU's identifier*/
	public static String getGPUName()
	{
		return GL11.glGetString(GL11.GL_RENDERER);
	}
	
}
