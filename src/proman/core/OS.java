package proman.core;

public enum OS 
{
	WINDOWS,
	UNIX,
	MACOSX,
	SOLARIS,
	MISCELLAENOUS;
	
	public static OS current()
	{
		String osName = System.getProperty("os.name").toLowerCase();
		if		(osName.indexOf("win") >= 0)	return WINDOWS;
		else if	(osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0)	return UNIX;
		else if	(osName.indexOf("mac") >= 0 || osName.indexOf("darvin") >= 0)	return MACOSX;
		return MISCELLAENOUS;
	}
}
