package proman.font;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import proman.core.ManagerException;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.math.vector.Vec4i;
import proman.util.io.Address;

public class DivoFont extends Font
{
	public DivoFont(Address imageAddress, Address metricsAddress, float lineOffsetPerSize, boolean smoothed) throws ManagerException
	{
		super(imageAddress, smoothed, lineOffsetPerSize);
		try
		{
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(metricsAddress.openInputStream());
			
			int fontHeight = Integer.parseInt(((Element)doc.getElementsByTagName("Font").item(0)).getAttribute("height"));
			
			NodeList list = doc.getElementsByTagName("Char");
			for(int i=0;i<list.getLength();i++)
			{
				Element e = (Element)list.item(i);
				char character = e.getAttribute("code").charAt(0);
				
				if(character == '&')
					continue;
				
				String[] rect = e.getAttribute("rect").split(" ");
				Vec4i quad = new Vec4i(
						Integer.parseInt(rect[0]),
						Integer.parseInt(rect[1]),
						Integer.parseInt(rect[2]),
						Integer.parseInt(rect[3]));
				
				String[] offset = e.getAttribute("offset").split(" ");
				quad.y -= Integer.parseInt(offset[1]);
				quad.x -= Integer.parseInt(offset[0]);
				
				Vec2i texDimensions = this.sampler.dimensions();
				Vec4f texExcerpt = new Vec4f(
						quad.x/(float)texDimensions.x,
						1f-(quad.y+fontHeight)/(float)texDimensions.y,
						quad.z/(float)texDimensions.x,
						fontHeight/(float)texDimensions.y);
				this.chars.put(character, texExcerpt);
			}
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ManagerException("Unable to load font metrics");
		}
	}
	
	public DivoFont(Address imageAddress, Address metricsAddress, boolean smoothed) throws ManagerException
	{
		this(imageAddress, metricsAddress, 0, smoothed);
	}
}
