package proman.font;

import java.util.HashMap;

import proman.core.ManagerException;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec4f;
import proman.texture.Sampler2D;
import proman.texture.Texture;
import proman.util.io.Address;

public class Font 
{
	float lineOffsetPerSize=0f;
	public Sampler2D sampler;
	public HashMap<Character, Vec4f> chars;
	boolean smoothed;
	
	public Font(Sampler2D sampler, boolean smoothed, float lineOffsetPerSize)
	{
		this.smoothed = smoothed;
		this.lineOffsetPerSize = lineOffsetPerSize;
		this.sampler = sampler;
	}
	
	public Font(Address adress, boolean smoothed, float lineOffsetPerSize) throws ManagerException
	{
		this(adress, smoothed);
		this.lineOffsetPerSize = lineOffsetPerSize;
	}
	
	public Font(Address adress, boolean smoothed) throws ManagerException
	{
		this.smoothed = smoothed;
		this.chars = new HashMap<Character, Vec4f>();
		
		if(adress != null)
		{
			try
			{
				this.sampler = new Sampler2D(adress);
			}
			catch(ManagerException e)
			{
				throw new ManagerException("Unable to load font: "+e.getLocalizedMessage());
			}
		}
	}
	
	
	public Texture charTexture(Character character)
	{
		if(this.sampler == null)
			return null;
		
		if(this.chars.get(character) != null)
		{
			return new Texture(this.sampler, this.chars.get(character).clone(), this.smoothed);
		}
		else if((int)character<256)
		{
			int column 	= ((int)character)%16;
			int row		= ((int)character-column)/16+1;
			return new Texture(this.sampler, new Vec4f(column*(1f/16f), 1f-row*(1f/16f), 1f/16f, 1f/16f), this.smoothed);
		}
		return new Texture(this.sampler, new Vec4f(0, 0, 0, 0), this.smoothed);
	}
	
	public float getLineOffset()
	{
		return this.lineOffsetPerSize;
	}
	
	public float getCharWidth(char c)
	{
		Vec2f[] vecs = this.charTexture(c).getTextureExcerpts();
		return( 
				((vecs[1].x-vecs[0].x)*this.sampler.dimensions().x )/
				((vecs[2].y-vecs[1].y)*this.sampler.dimensions().y ));
	}
	
	public float getStringWidth(String string)
	{
		float width = 0;
		for(char c : string.toCharArray())
			width += getCharWidth(c);
		return width;
	}
	
}
