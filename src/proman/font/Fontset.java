package proman.font;

import java.util.HashMap;


public abstract class Fontset 
{
	
	
	private static HashMap<String, Font> fonts = new HashMap<String, Font>();
	
	
	public static void addFont(Font font, String id)
	{
		fonts.put(id, font);
	}
	
	public static Font get(String id)
	{
		return fonts.get(id);
	}
	
}
