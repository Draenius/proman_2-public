package proman.shader;

import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import proman.core.ManagerException;
import proman.math.vector.Vec;
import proman.math.vector.Vec1f;
import proman.math.vector.Vec1i;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec3f;
import proman.math.vector.Vec3i;
import proman.math.vector.Vec4f;
import proman.math.vector.Vec4i;
import proman.util.io.Address;
import proman.util.io.IOUtil;

public class GL2ShaderProgram 
{
	int vertID;
	int fragID;
	
	int id;
	
	HashMap<String, Integer> attributeLocations = new HashMap<String, Integer>();
	HashMap<String, Integer> uniformLocations 	= new HashMap<String, Integer>();
	
	public GL2ShaderProgram(Address vertexShader, Address fragmentShader, String...attributes) throws ManagerException
	{
		this(	IOUtil.readFile(vertexShader), 
				IOUtil.readFile(fragmentShader), 
				attributes);
	}
	
	public GL2ShaderProgram(String vertexShader, String fragmentShader, String...attributes) throws ManagerException
	{
		this.id = GL20.glCreateProgram();
		this.vertID = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		GL20.glShaderSource(this.vertID, vertexShader);
		GL20.glCompileShader(this.vertID);
		
		if (GL20.glGetShaderi(this.vertID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
			throw new ManagerException("Unable to compile vertex shader \n"+log(this.vertID));
		
		
		this.fragID = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
		GL20.glShaderSource(this.fragID, fragmentShader);
		GL20.glCompileShader(this.fragID);
		
		if (GL20.glGetShaderi(this.fragID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
			throw new ManagerException("Unable to compile fragment shader \n"+log(this.fragID));
		
		
		GL20.glAttachShader(this.id, this.vertID);
		GL20.glAttachShader(this.id, this.fragID);
		
		/*for(int i=0;i<attributes.length;i++)
		{
			GL20.glBindAttribLocation(this.id, i, attributes[i]);
			this.attributeLocations.put(attributes[i], i);
		}*/
		
		GL20.glLinkProgram(this.id);
		GL20.glValidateProgram(this.id);
		
		/*if (GL20.glGetShaderi(this.id, GL20.GL_LINK_STATUS) == GL11.GL_FALSE)
			throw new ManagerException("Unable to link program: "+log(this.id));
		if (GL20.glGetShaderi(this.id, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE)
			throw new ManagerException("Unable to validate program: "+log(this.id));
		*/
	}
	
	
	public void apply()
	{
		if(this.id > 0)
		{
			GL20.glUseProgram(this.id);
		}
	}
	
	public void applyVertexAttribute(String id, Vec<?> value)
	{
		int location = attributeLocation(id);
		if(location <= 0) return;
				
		if(value instanceof Vec4f) 
			{ Vec4f vec4 = (Vec4f)value; GL20.glVertexAttrib4f(location, vec4.x, vec4.y, vec4.z, vec4.w);}
		else if(value instanceof Vec3f) 
			{ Vec3f vec3 = (Vec3f)value; GL20.glVertexAttrib3f(location, vec3.x, vec3.y, vec3.z);}
		else if(value instanceof Vec2f) 
			{ Vec2f vec2 = (Vec2f)value; GL20.glVertexAttrib2f(location, vec2.x, vec2.y);}
		else if(value instanceof Vec1f) 
			{ Vec1f vec1 = (Vec1f)value; GL20.glVertexAttrib1f(location, vec1.x);}
	}
	
	public void applyUniform(String id, int value)
	{
		int location = uniformLocation(id);
		if(location <= 0) return;
		GL20.glUniform1i(location, value);
	}
	public void applyUniform(String id, float value)
	{
		int location = uniformLocation(id);
		if(location <= 0) return;
		GL20.glUniform1f(location, value);
	}
	public void applyUniform(String id, Vec<?> value)
	{
		int location = uniformLocation(id);
		if(location <= 0) return;
		
		if(value instanceof Vec4f) 
			{ Vec4f vec4 = (Vec4f)value; GL20.glUniform4f(location, vec4.x, vec4.y, vec4.z, vec4.w);}
		else if(value instanceof Vec3f) 
			{ Vec3f vec3 = (Vec3f)value; GL20.glUniform3f(location, vec3.x, vec3.y, vec3.z);}
		else if(value instanceof Vec2f) 
			{ Vec2f vec2 = (Vec2f)value; GL20.glUniform2f(location, vec2.x, vec2.y);}
		else if(value instanceof Vec1f) 
			{ Vec1f vec1 = (Vec1f)value; GL20.glUniform1f(location, vec1.x);}
		
			if(value instanceof Vec4i) 
			{ Vec4i vec4 = (Vec4i)value; GL20.glUniform4i(location, vec4.x, vec4.y, vec4.z, vec4.w);}
		else if(value instanceof Vec3i) 
			{ Vec3i vec3 = (Vec3i)value; GL20.glUniform3i(location, vec3.x, vec3.y, vec3.z);}
		else if(value instanceof Vec2i) 
			{ Vec2i vec2 = (Vec2i)value; GL20.glUniform2i(location, vec2.x, vec2.y);}
		else if(value instanceof Vec1i) 
			{ Vec1i vec1 = (Vec1i)value; GL20.glUniform1i(location, vec1.x);}
	}
	
	private int uniformLocation(String id)
	{
		if(this.uniformLocations.containsKey(id))
			return this.uniformLocations.get(id);
		else
		{
			int location = GL20.glGetUniformLocation(this.id, id);
			this.uniformLocations.put(id, location);
			//System.out.println(id+" "+location);
			return location;
		}
		
	}
	
	public int attributeLocation(String id)
	{
		if(this.attributeLocations.containsKey(id))
			return this.attributeLocations.get(id);
		else
		{
			int location = GL20.glGetAttribLocation(this.id, id);
			this.attributeLocations.put(id, location);
			return location;
		}

		
	}
	
	public static void useDefault()
	{
		GL20.glUseProgram(0);
	}
	
	
	public void dispose()
	{
		GL20.glUseProgram(0);
		GL20.glDetachShader(this.id, this.vertID);
		GL20.glDetachShader(this.id, this.fragID);
		
		GL20.glDeleteShader(this.vertID);
		GL20.glDeleteShader(this.fragID);
		GL20.glDeleteProgram(this.id);
		
		this.vertID = 0;
		this.fragID = 0;
		this.id = 0;
	}
	
	private static String log(int id)
	{
		return GL20.glGetShaderInfoLog(id);
	}
}
