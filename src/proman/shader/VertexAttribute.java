package proman.shader;

import org.lwjgl.opengl.GL20;

import proman.math.vector.Vec;
import proman.math.vector.Vec1f;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec3f;
import proman.math.vector.Vec4f;

public class VertexAttribute 
{
	int handle;
	boolean usable;
	
	VertexAttribute(String id, int programHandle)
	{
		this.handle  = GL20.glGetAttribLocation(programHandle, id);
		this.usable = this.handle > 0;
	}
	
	public void set(Vec<?> Vec)
	{
		if(this.handle <= 0) return;

		 		if(Vec instanceof Vec4f)	{	Vec4f vec = (Vec4f) Vec;	GL20.glVertexAttrib4f(this.handle, vec.x, vec.y, vec.z, vec.w); return;}
		else 	if(Vec instanceof Vec3f)	{	Vec3f vec = (Vec3f) Vec;	GL20.glVertexAttrib3f(this.handle, vec.x, vec.y, vec.z); return;}
		else 	if(Vec instanceof Vec2f)	{	Vec2f vec = (Vec2f) Vec;	GL20.glVertexAttrib2f(this.handle, vec.x, vec.y); return;}
		else	if(Vec instanceof Vec1f)	{	Vec1f vec = (Vec1f) Vec;	GL20.glVertexAttrib1f(this.handle, vec.x); return;}
	}
	
	public int getHandle()
	{
		return this.handle;
	}
}
