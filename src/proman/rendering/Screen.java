package proman.rendering;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glViewport;

import org.lwjgl.opengl.GL11;

import proman.core.Core;
import proman.core.Display;
import proman.core.ManagerException;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.math.vector.Vec4i;
import proman.util.MathUtil;

public class Screen 
{
	private Vec2f aspect;
	Renderer renderer;
	
	private boolean applied = true;
	
	private Vec2f maxStretch = new Vec2f(1.2f, 1.2f);
	private Vec2f maxExtension = new Vec2f(1.2f, 1.2f);
	
	public Screen(Vec2f aspect)
	{
		this.aspect = aspect.clone();
	}
	
	public void setAsCurrent()
	{
		Core.setCurrentScreen(this);
		this.applied = true;
	}
	
	public void update() throws ManagerException
	{
		if(this.renderer != null)
			this.renderer.update();
	}
	
	public void applyViewportForResolution(Vec2i resolution)
	{
		Vec4i viewportCoords = this.getViewportCoords(resolution);
		Vec4f sC = this.getVisibleArea(resolution);
		
		glMatrixMode(GL_PROJECTION);
		GL11.glLoadIdentity();
		glViewport(viewportCoords.x, viewportCoords.y, viewportCoords.z, viewportCoords.w);
		glMatrixMode(GL_MODELVIEW);
		
		glMatrixMode(GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(sC.x, sC.x+sC.z, sC.y, sC.y+sC.w, -1, 1);
		glMatrixMode(GL_MODELVIEW);
		
	}
	
	public void render() throws ManagerException
	{
		if(Display.wasResized() || this.applied)
		{
			this.applied = false;
			this.applyViewportForResolution(Display.getSize());
		}
			
		if(this.renderer != null)
		{
			this.renderer.render();
		}
				
	}
	
	public Vec4f getVisibleArea()
	{
		return this.getVisibleArea(Display.getSize());
	}
	
	public Vec4f getVisibleArea(Vec2i displaySize)
	{
		Vec2f aspect = this.getAspect();
		Vec2f relSize;
		if(displaySize.y/aspect.y >= displaySize.x/aspect.x)
			relSize = new Vec2f(1, (displaySize.y/(displaySize.x/aspect.x))/aspect.y);
		else
			relSize = new Vec2f((displaySize.x/(displaySize.y/aspect.y))/aspect.x, 1);
			
		Vec2f overhang = new Vec2f(
				MathUtil.min(relSize.x-1f, this.maxExtension.x-1f),
				MathUtil.min(relSize.y-1f, this.maxExtension.y-1f));
		
		Vec4f bounds = new Vec4f(
				-overhang.x*.5f*aspect.x, -overhang.y*.5f*aspect.y, 
				aspect.x+(aspect.x*overhang.x), aspect.y+(aspect.y*overhang.y));
		
		return bounds;
	}
	
	public Vec4i getViewportCoords(Vec2i displaySize)
	{
		Vec4f bSC = this.getVisibleArea(displaySize);
		Vec2f aspect = this.getAspect().add(new Vec2f(-bSC.x*2f, -bSC.y*2f));
		
		boolean vShrink = displaySize.y/aspect.y >= displaySize.x/aspect.x;
		Vec2f maxRes = new Vec2f(
				(vShrink ? displaySize.x : (displaySize.y/aspect.y)*aspect.x),
				(vShrink ? (displaySize.x/aspect.x)*aspect.y : displaySize.y));
		Vec2i resolution = new Vec2i(
				(int) MathUtil.min(maxRes.x*this.maxStretch.x, displaySize.x), 
				(int) MathUtil.min(maxRes.y*this.maxStretch.y, displaySize.y));
		return new Vec4i((displaySize.x-resolution.x)/2, (displaySize.y-resolution.y)/2, 
					resolution.x, resolution.y);
	}
	
	public void setRenderer(Renderer content)
	{
		this.renderer = content;
	}
		
		
	public Vec2f getAspect()
	{
		return this.aspect.clone();
	}
	
	public Vec2i getDisplaySize()
	{
		Vec4i viewportCoords = this.getViewportCoords(Display.getSize());
		return new Vec2i(viewportCoords.z, viewportCoords.w);
	}
		
	public Renderer getCurrentRenderer()
	{
		return this.renderer;
	}
	
	public void setMaxExtension(float horizontal, float vertical)
	{
		this.maxExtension = new Vec2f(horizontal, vertical);
	}
	
	public void setMaxExpansion(float horizontal, float vertical)
	{
		this.maxStretch = new Vec2f(horizontal, vertical);
	}
	
	public Vec2f getMaxExtension()	{	return this.maxExtension.clone();}
	public Vec2f getMaxExpansion()	{	return this.maxStretch.clone();}
}
