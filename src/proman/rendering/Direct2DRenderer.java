package proman.rendering;

import proman.font.Font;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.texture.Texture;
import proman.util.Color;

public abstract class Direct2DRenderer extends Renderer
{
	public abstract void pushViewport();
	public abstract void popViewport();
	public abstract void translateViewport(float x, float y);
	public abstract void rotateViewport(float angle);
	
	public abstract void drawQuads(float[] vertices, float[] colors, float[] texCoords, Texture texture);
	public abstract void drawTriangles(float[] vertices, float[] colors, float[] texCoords, Texture texture);
	
	public abstract void drawBow(Vec2f center, float innerRadius, float outerRadius, float startAngle, float endAngle, int detail, Color innerColor, Color outerColor);
	
	public void drawQuad(Vec4f quad, Color color)
	{
		this.drawQuad(quad, color, null);
	}
	
	public void drawQuad(Vec4f quad, Color color, Texture texture)
	{
		this.drawQuad(quad, new Color[]{color, color, color, color}, texture);
	}
	
	public void drawQuad(Vec4f quad, Color[] colors, Texture texture)
	{
		float[] lights = new float[4*4];
		Vec2f[] texCoords = texture == null ? new Vec2f[0] : texture.getTextureExcerpts();
		float[] texPos = new float[texCoords.length*2];
		for(int i=0;i<4;i++)
		{
			lights[4*i+0] = colors[i].x;
			lights[4*i+1] = colors[i].y;
			lights[4*i+2] = colors[i].z;
			lights[4*i+3] = colors[i].w;
			if(i < texCoords.length)
			{
				texPos[i*2+0] = texCoords[i].x;
				texPos[i*2+1] = texCoords[i].y;
			}
		}
		
		this.drawQuads(
				new float[]
					{quad.x, quad.y, quad.x+quad.z, quad.y, quad.x+quad.z, quad.y+quad.w, quad.x, quad.y+quad.w}, 
				lights, 
				texPos, 
				texture);
	}
	
	public void drawQuads(Vec4f[] quads, Color[] colors, Texture texture)
	{
		int[] fac = new int[]{0, 0, 0, 1, 1, 1, 1, 0};
		float[] coords = new float[quads.length*2*4];
		float[] texCoords = new float[texture != null ? quads.length*2*4 : 0];
		float[] lights = new float[quads.length*4*4];
		Vec2f[] exc = texture != null  ? texture.getTextureExcerpts() : new Vec2f[0];
		for(int i=0;i<quads.length;i++)
		{
			Vec4f quad = quads[i];
			for(int j=0;j<4;j++)
			{
				coords[i*4*2+j*2+0] = quad.x+fac[j*2+0]*quad.z;
				coords[i*4*2+j*2+1] = quad.y+fac[j*2+1]*quad.w;
				if(texture != null)
				{
					texCoords[i*4*2+j*2+0] = exc[j<exc.length ? j : exc.length-1].x;
					texCoords[i*4*2+j*2+1] = exc[j<exc.length ? j : exc.length-1].y;
				}
			}
			Color color = colors[i];
			float[] colorLights = new float[]{color.x, color.y, color.z, color.w};
			for(int j=0;j<4;j++)
				for(int k=0;k<4;k++)
					lights[i*4*4+j*4+k] = colorLights[k];
		}
		this.drawQuads(coords, lights, texCoords, texture);
	}

	public void drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, Color color)
	{
		this.drawTriangles(
				new float[]{x1, y1, x2, y2, x3, y3}, 
				new float[]{color.x, color.y, color.z, color.w, color.x, color.y, color.z, color.w, color.x, color.y, color.z, color.w}, 
				null, null);
	}
	
	public void drawTriangle(Vec2f p1, Vec2f p2, Vec2f p3, Color color)
	{
		this.drawTriangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, color);
	}
	
	
	public void drawCircle(Vec2f pos, float radius, int detail, Color color)
	{
		this.drawCircle(pos, radius, detail, color, color);
	}
	
	public void drawCircle(Vec2f pos, float radius, int detail, Color innerColor, Color outerColor)
	{
		this.drawRing(pos, 0,  radius, detail, innerColor, outerColor);
	}
	
	public void drawRing(Vec2f pos, float innerRadius, float outerRadius, int detail, Color color)
	{
		this.drawRing(pos, innerRadius,  outerRadius, detail, color, color);
	}

	public void drawRing(Vec2f pos, float innerRadius, float outerRadius, int detail, Color innerColor, Color outerColor)
	{
		this.drawBow(pos, innerRadius,  outerRadius, 0f, 360f, detail, innerColor, outerColor);
	}
	
	public void drawBow(Vec2f center, float innerRadius, float outerRadius, float startAngle, float endAngle, int detail, Color color)
	{
		this.drawBow(center, innerRadius,  outerRadius, startAngle, endAngle, detail, color, color);
	}

	public void drawString(String string, Font font, Vec2f pos, float size, Color color)
	{
		this.drawString(string, font, pos, size, 1f, color);
	}
	
	public void drawString(String string, Font font, Vec2f pos, float size, float stretch, Color color)
	{
		pos = pos.clone();
		Vec2f pointer = pos.clone();
		pointer.y -= size;
		
		for(int i=0;i<string.length();i++)
		{
			if(string.charAt(i) != '\n')
			{
				Texture character = font.charTexture(string.charAt(i));
				
				Vec2f[] vecs = character.getTextureExcerpts();
				
				float height = size;
				Vec2i dim = font.sampler.dimensions();
				float width = ( ((vecs[1].x-vecs[0].x)*dim.x ) / ((vecs[2].y-vecs[1].y)*dim.y ))*size*stretch;
				this.drawQuad(new Vec4f(pointer.x, pointer.y, width, height), 
						color, character);
				pointer.x += width;
			}
			else
			{
				pointer.x = pos.x;
				pointer.y -= size+font.getLineOffset()*size;
			}
		}
	}


}
