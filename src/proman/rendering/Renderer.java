package proman.rendering;

public abstract class Renderer 
{
	public abstract void render();
	public abstract void update();
}
