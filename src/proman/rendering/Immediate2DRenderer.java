package proman.rendering;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import org.lwjgl.opengl.GL11;

import proman.math.vector.Vec2f;
import proman.texture.Sampler2D;
import proman.texture.Texture;
import proman.util.Color;
import proman.util.MathUtil;

public abstract class Immediate2DRenderer extends Direct2DRenderer
{

	@Override	public void pushViewport()	{	GL11.glPushMatrix();}
	@Override	public void popViewport()	{	GL11.glPopMatrix();}
	@Override	public void rotateViewport(float angle)			{	GL11.glRotatef(-angle, 0, 0, 1);}
	@Override	public void translateViewport(float x, float y)	{	GL11.glTranslatef(x, y, 0);}

	@Override
	public void drawQuads(float[] vertices, float[] colors, float[] texCoords, Texture texture)
	{
		if(vertices==null) 	return;
		if(colors==null)
		{
			colors = new float[0];
			glColor4f(1, 1, 1, 1);
		}
		
		if(texCoords==null || texture==null)
		{
			Sampler2D.releaseAny();
			texCoords = new float[0];
		}
		else
			texture.bind();
		
		glBegin(GL_QUADS);
		for(int i=0; i<((vertices.length/2)/4)*4;i++)
		{
			if(i*2+1<texCoords.length)	
				glTexCoord2f(texCoords[i*2], 1f-texCoords[i*2+1]);
			if(i*4+3<colors.length)	
				glColor4f(colors[i*4], colors[i*4+1], colors[i*4+2], colors[i*4+3]);
			glVertex2f(vertices[i*2+0], vertices[i*2+1]);
		}
		glEnd();

	}

	@Override
	public void drawTriangles(float[] vertices, float[] colors, float[] texCoords, Texture texture)
	{
		if(vertices==null) 	return;
		if(colors==null)
		{
			colors = new float[0];
			glColor4f(1, 1, 1, 1);
		}
		
		if(texCoords==null || texture==null)
		{
			Sampler2D.releaseAny();
			texCoords = new float[0];
		}
		else
			texture.bind();
		
		glBegin(GL_TRIANGLES);
		for(int i=0; i<((vertices.length/2)/3)*3;i++)
		{
			if(i*2+1<texCoords.length)	
				glTexCoord2f(texCoords[i], 1-texCoords[i+1]);
			if(i*4+3<colors.length)	
				glColor4f(colors[i*4], colors[i*4+1], colors[i*4+2], colors[i*4+3]);
			glVertex2f(vertices[i*2+0], vertices[i*2+1]);
		}
		glEnd();
		
	}


	public void drawBow(Vec2f center, float innerRadius, float outerRadius, float startAngle, float endAngle, int detail, Color innerColor, Color outerColor)
	{
		Sampler2D.releaseAny();
		
		glBegin(GL11.GL_QUADS);
		{
			Color 	in = innerColor != null ? innerColor : Color.WHITE,
					out = outerColor != null ? outerColor : Color.WHITE;
			for(int i=0;i<detail;i++)
			{
				float 
				sin1 = MathUtil.sin(startAngle+(-(startAngle-endAngle)/detail)*(i+0)), 
				sin2 = MathUtil.sin(startAngle+(-(startAngle-endAngle)/detail)*(i+1)), 
				cos1 = MathUtil.cos(startAngle+(-(startAngle-endAngle)/detail)*(i+0)), 
				cos2 = MathUtil.cos(startAngle+(-(startAngle-endAngle)/detail)*(i+1));
				
				glColor4f(in.x, in.y, in.z, in.w);
				glVertex2f(center.x+sin1*innerRadius, center.y+cos1*innerRadius);
				glColor4f(out.x, out.y, out.z, out.w);
				glVertex2f(center.x+sin1*outerRadius, center.y+cos1*outerRadius);
				glVertex2f(center.x+sin2*outerRadius, center.y+cos2*outerRadius);
				glColor4f(in.x, in.y, in.z, in.w);
				glVertex2f(center.x+sin2*innerRadius, center.y+cos2*innerRadius);
			}
		}
		glEnd();
	}
	
	@Override
	public void drawCircle(Vec2f center, float radius, int detail, Color innerColor, Color outerColor)
	{
		Sampler2D.releaseAny();
		if(innerColor == null)
			glColor4f(1, 1, 1, 1);
		else
			glColor4f(innerColor.x, innerColor.y, innerColor.z, innerColor.w);
		
		glBegin(GL11.GL_TRIANGLE_FAN);
		{
			if(innerColor == null)
				glColor4f(1, 1, 1, 1);
			else
				glColor4f(innerColor.x, innerColor.y, innerColor.z, innerColor.w);
			
			glVertex2f(center.x, center.y);
			
			if(outerColor == null)
				glColor4f(1, 1, 1, 1);
			else
				glColor4f(outerColor.x, outerColor.y, outerColor.z, outerColor.w);
			
			for(int i=0;i<=detail;i++)
			{
				float angle = (360f/detail)*i;
				glVertex2f(center.x+MathUtil.sin(angle)*radius, center.y+MathUtil.cos(angle)*radius);
			}
		}
		glEnd();
	}
	
}
