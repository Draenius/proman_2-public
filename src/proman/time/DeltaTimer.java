package proman.time;

public class DeltaTimer extends Stopwatch
{
	long lastFrame;
	
	public DeltaTimer()
	{
		super();
		this.lastFrame = this.getNanos();
	}
	
	public double newestDelta()
	{
		long now = this.getNanos();
		long delta = now-this.lastFrame;
		this.lastFrame = now;
		
		return delta/1000000000d;
	}
}
