package proman.time;

public abstract class Timer 
{
	public abstract void pause();
	public abstract void unpause();
	public abstract boolean isPaused();
	public abstract void reset();
}

