package proman.time;

public class Time 
{
	public static long millis()
	{
		return System.currentTimeMillis();
	}
	
	public static long nanos()
	{
		return System.nanoTime();
	}
	
}
