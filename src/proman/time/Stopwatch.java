package proman.time;

public class Stopwatch extends Timer
{
	long init;
	long pause;
	long takeOff = 0;
	
	boolean paused;
	
	
	public Stopwatch()
	{
		this.init = Time.nanos();
	}
	
	
	public void reset()
	{
		this.init = Time.nanos();
		this.pause = Time.nanos();
		this.takeOff = 0;
	}
	
	public void pause() 
	{
		if(!this.paused)
		{
			this.pause = Time.nanos();
			this.paused = true;
		}
	}

	public void unpause() 
	{
		if(this.paused)
		{
			this.takeOff += Time.nanos()-this.pause;
			this.paused = false;
		}
	}
	
	public long getNanos()
	{
		return Time.nanos()-this.init-this.takeOff-(this.paused ? Time.nanos()-this.pause : 0);
	}
	
	public double getSeconds()
	{
		return this.getNanos()/1000000000d;
	}
	
	public boolean isPaused()
	{
		return this.paused;
	}
}
