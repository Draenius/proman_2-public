package proman.time;

public class DeltaFrameTimer extends DeltaTimer
{
	double overflow;
	
	public DeltaFrameTimer()
	{
		super();
	}
	
	public int getNewFrames(double frameStep)
	{
		double newDelta = this.newestDelta();
		int newFrames = (int) ((newDelta+this.overflow)/frameStep);
		this.overflow = (newDelta+this.overflow)-(newFrames*frameStep);
		return newFrames;
	}
}
