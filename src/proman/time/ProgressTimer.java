package proman.time;

import proman.util.MathUtil;

public class ProgressTimer extends Timer
{
	long init;
	long pause;
	long takeOff;
	
	boolean paused;
	
	double secondsToRun;
	
	Stopwatch source;
	
	public ProgressTimer(double secondsToRun)
	{
		this.init = Time.nanos();
		this.secondsToRun = secondsToRun;
	}
	
	public ProgressTimer(double secondsToRun, Stopwatch source)
	{
		this.source = source;
		this.init = source.getNanos();
		this.secondsToRun = secondsToRun;
	}

	
	public void reset()
	{
		this.init = nanos();
		this.pause = nanos();
		this.takeOff = 0;
	}
	
	
	public ProgressTimer paused()
	{
		this.pause();
		return this;
	}
	
	public void pause() 
	{
		if(!this.paused)
		{
			this.pause = nanos();
			this.paused = true;
		}
	}

	public void unpause() 
	{
		if(this.paused)
		{
			this.takeOff += nanos()-this.pause;
			this.paused = false;
		}
	}
	
	private long getMillis()
	{
		return nanos()-this.init-this.takeOff-(this.paused ? nanos()-this.pause : 0);
	}
	
	public double getProgress()
	{
		return MathUtil.min((this.getMillis()/1000000000d)/this.secondsToRun, 1d);
	}
	
	public boolean completed()
	{
		return this.getProgress() >= 1d;
	}
	
	public boolean isPaused()
	{
		return this.paused;
	}
	
	/*private long millis()
	{
		return this.source != null ? this.source.getMillis() : Time.millis();
	}*/
	private long nanos()
	{
		return this.source != null ? this.source.getNanos() : Time.nanos();
	}
	
	public void complete()
	{
		this.setProgress(1);
	}
	
	public void setProgress(double progress)
	{
		this.init = this.nanos()-(long)(progress * this.secondsToRun * 1000000000);
		this.pause = this.nanos();
		this.takeOff = 0;
	}
}
