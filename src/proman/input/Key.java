package proman.input;

import org.lwjgl.glfw.GLFW;

public enum Key 
{
	STD0(GLFW.GLFW_KEY_0),
	STD1(GLFW.GLFW_KEY_1),
	STD2(GLFW.GLFW_KEY_2),
	STD3(GLFW.GLFW_KEY_3),
	STD4(GLFW.GLFW_KEY_4),
	STD5(GLFW.GLFW_KEY_5),
	STD6(GLFW.GLFW_KEY_6),
	STD7(GLFW.GLFW_KEY_7),
	STD8(GLFW.GLFW_KEY_8),
	STD9(GLFW.GLFW_KEY_9),
	
	A(GLFW.GLFW_KEY_A),
	B(GLFW.GLFW_KEY_B),
	C(GLFW.GLFW_KEY_C),
	D(GLFW.GLFW_KEY_D),
	E(GLFW.GLFW_KEY_E),
	F(GLFW.GLFW_KEY_F),
	G(GLFW.GLFW_KEY_G),
	H(GLFW.GLFW_KEY_H),
	I(GLFW.GLFW_KEY_I),
	J(GLFW.GLFW_KEY_J),
	K(GLFW.GLFW_KEY_K),
	L(GLFW.GLFW_KEY_L),
	M(GLFW.GLFW_KEY_M),
	N(GLFW.GLFW_KEY_N),
	O(GLFW.GLFW_KEY_O),
	P(GLFW.GLFW_KEY_P),
	Q(GLFW.GLFW_KEY_Q),
	R(GLFW.GLFW_KEY_R),
	S(GLFW.GLFW_KEY_S),
	T(GLFW.GLFW_KEY_T),
	U(GLFW.GLFW_KEY_U),
	V(GLFW.GLFW_KEY_V),
	W(GLFW.GLFW_KEY_W),
	X(GLFW.GLFW_KEY_X),
	Y(GLFW.GLFW_KEY_Y),
	Z(GLFW.GLFW_KEY_Z),
	
	F1(GLFW.GLFW_KEY_F1),
	F2(GLFW.GLFW_KEY_F2),
	F3(GLFW.GLFW_KEY_F3),
	F4(GLFW.GLFW_KEY_F4),
	F5(GLFW.GLFW_KEY_F5),
	F6(GLFW.GLFW_KEY_F6),
	F7(GLFW.GLFW_KEY_F7),
	F8(GLFW.GLFW_KEY_F8),
	F9(GLFW.GLFW_KEY_F9),
	F10(GLFW.GLFW_KEY_F10),
	F11(GLFW.GLFW_KEY_F11),
	F12(GLFW.GLFW_KEY_F12),

	LEFT(GLFW.GLFW_KEY_LEFT),
	RIGHT(GLFW.GLFW_KEY_RIGHT),
	UP(GLFW.GLFW_KEY_UP),
	DOWN(GLFW.GLFW_KEY_DOWN),
	
	LSHIFT(GLFW.GLFW_KEY_LEFT_SHIFT),
	RSHIFT(GLFW.GLFW_KEY_RIGHT_SHIFT),
	LCTRL(GLFW.GLFW_KEY_LEFT_CONTROL),
	RCTRL(GLFW.GLFW_KEY_RIGHT_CONTROL),
	ENTER(GLFW.GLFW_KEY_ENTER),
	ESC(GLFW.GLFW_KEY_ESCAPE),
	BACKSPACE(GLFW.GLFW_KEY_BACKSPACE),
	SPACE(GLFW.GLFW_KEY_SPACE),
	TAB(GLFW.GLFW_KEY_TAB),
	PAUSE(GLFW.GLFW_KEY_PAUSE),
	DELETE(GLFW.GLFW_KEY_DELETE),
	INSERT(GLFW.GLFW_KEY_INSERT),
	
	PERIOD(GLFW.GLFW_KEY_PERIOD),
	//COLON(GLFW.GLFW_KEY_COLON),
	COMMA(GLFW.GLFW_KEY_COMMA);
	
	public boolean wasTyped()	{	return proman.input.Keyboard.wasKeyTyped(this);}
	public boolean wasReleased(){	return proman.input.Keyboard.wasKeyReleased(this);}
	public boolean wasRepeated(){	return proman.input.Keyboard.wasKeyRepeated(this);}
	public boolean isDown()		{	return proman.input.Keyboard.isKeyDown(this);}
	
	
	private int id;
	private Key(int id)
	{
		this.id = id;
	}
	public int id()
	{
		return this.id;
	}
	
	public static Key getByID(int id)
	{
		for(Key key : Key.values())
			if(key.id == id)
				return key;
		return null;
	}
}
