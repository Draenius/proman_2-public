package proman.input;

import org.lwjgl.glfw.GLFW;

public enum MouseButton 
{
	LEFT(GLFW.GLFW_MOUSE_BUTTON_1),
	RIGHT(GLFW.GLFW_MOUSE_BUTTON_2),
	CENTER(GLFW.GLFW_MOUSE_BUTTON_3),
	BACK(GLFW.GLFW_MOUSE_BUTTON_4),
	FORWARD(GLFW.GLFW_MOUSE_BUTTON_5);
	
	private int id;
	
	private MouseButton(int id)
	{
		this.id = id;
	}
	public int id()
	{
		return this.id;
	}

	public boolean wasClicked()	{	return proman.input.Mouse.wasButtonClicked(this);}
	public boolean wasReleased(){	return proman.input.Mouse.wasButtonReleased(this);}
	public boolean isDown()		{	return proman.input.Mouse.isButtonDown(this);}
	
}
