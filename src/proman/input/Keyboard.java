package proman.input;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCharCallbackI;
import org.lwjgl.glfw.GLFWKeyCallback;

import proman.core.Display;


public class Keyboard 
{
	private static ArrayList<Integer> pressedKeys = new ArrayList<Integer>();
	private static ArrayList<Integer> releasedKeys = new ArrayList<Integer>();
	private static ArrayList<Integer> repeatedKeys = new ArrayList<Integer>();
	private static String pressedChars = "";
	
	private static boolean created = false;
	public static void create()
	{
		GLFWKeyCallback keyCallback = new GLFWKeyCallback()
		{
			@Override public void invoke(long window, int key, int scancode, int action, int mods)
			{
				switch(action)
				{
				case GLFW.GLFW_PRESS: pressedKeys.add(key); break;
				case GLFW.GLFW_RELEASE: releasedKeys.add(key); break;
				case GLFW.GLFW_REPEAT: repeatedKeys.add(key); break;
				}
			}
		};
		GLFW.glfwSetKeyCallback(Display.getGlfwWindowHandle(), keyCallback);
		
		GLFWCharCallbackI charCallback = new GLFWCharCallbackI()
		{
			@Override public void invoke(long window, int codepoint)
			{
				pressedChars += (char)codepoint;//System.out.println(codepoint+": "+(char)codepoint);
			}
		};
		
		GLFW.glfwSetCharCallback(Display.getGlfwWindowHandle(), charCallback);
		
		created = true;
	}
	
	public static void update()
	{
		if(!created)
			create();
		
		pressedKeys.clear();
		releasedKeys.clear();
		repeatedKeys.clear();
		pressedChars = "";
		
	}
	
	public static boolean isKeyDown(Key key)
	{
		return GLFW.glfwGetKey(Display.getGlfwWindowHandle(), key.id()) == GLFW.GLFW_PRESS;
	}
	
	public static boolean wasKeyTyped(Key key)
	{
		for(int i=0;i<pressedKeys.size();i++)
			if(pressedKeys.get(i) == key.id()) return true;
		return false;
	}
	
	public static boolean wasKeyReleased(Key key)
	{
		for(int i=0;i<releasedKeys.size();i++)
			if(releasedKeys.get(i) == key.id()) return true;
		return false;
	}
	
	public static boolean wasKeyRepeated(Key key)
	{
		for(int i=0;i<repeatedKeys.size();i++)
			if(repeatedKeys.get(i) == key.id()) return true;
		return false;
	}
	
	public static String pressedChars()
	{
		return pressedChars;
	}
	
	public static boolean wasKeyTyped()
	{
		return pressedKeys.size() > 0;
	}
	
	public static boolean wasKeyReleased()
	{
		return releasedKeys.size() > 0;
	}
	
	public static boolean wasKeyRepeated()
	{
		return repeatedKeys.size() > 0;
	}
	
	public static Key[] getPressedKeys()
	{
		Key[] keys = new Key[pressedKeys.size()];
		for(int i=0;i<pressedKeys.size();i++)
		{
			int keyID = pressedKeys.get(i);
			for(Key key : Key.values())
				if(key.id() == keyID)
					keys[i] = key;
		}
		return keys;
	}
}
