package proman.input;

import java.nio.DoubleBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallbackI;

import proman.core.Core;
import proman.core.Display;
import proman.math.vector.Vec2f;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.math.vector.Vec4i;
import proman.rendering.Screen;

public class Mouse 
{
	private static ArrayList<Integer> pressedButtons = new ArrayList<Integer>();
	private static ArrayList<Integer> releasedButtons = new ArrayList<Integer>();
	private static int scrollWheelDelta;
	
	private static boolean created;
	private static DoubleBuffer posX, posY;
	
	public static void create()
	{
		posX = BufferUtils.createDoubleBuffer(1);
		posY = BufferUtils.createDoubleBuffer(1);
		
		GLFWMouseButtonCallback buttonCallback = new GLFWMouseButtonCallback()
		{
			@Override public void invoke(long window, int button, int action, int mods)
			{
				if(action == GLFW.GLFW_PRESS)
					pressedButtons.add(button);
				else
					releasedButtons.add(button);
			}
		};
		
		GLFWScrollCallbackI scrollCallback = new GLFWScrollCallbackI()
		{
			@Override public void invoke(long window, double x, double y)
			{
				scrollWheelDelta = (int)y;
			}
		};
		
		GLFW.glfwSetMouseButtonCallback(Display.getGlfwWindowHandle(), buttonCallback);
		GLFW.glfwSetScrollCallback(Display.getGlfwWindowHandle(), scrollCallback);
		
		created = true;
	}
	
	public static void update()
	{
		
		if(!created)
			create();
		
		/*try
		{
			if(!org.lwjgl.input.Mouse.isCreated())
				org.lwjgl.input.Mouse.create();
		} catch (Exception e){}*/
		
		pressedButtons.clear();
		releasedButtons.clear();
		scrollWheelDelta = 0;
		//GLFW.glfwgetm
		
		/*if(org.lwjgl.input.Mouse.isCreated())
		{
			scrollWheelDelta = org.lwjgl.input.Mouse.getDWheel();
			
			while(org.lwjgl.input.Mouse.next())
			{
				if(org.lwjgl.input.Mouse.getEventButtonState())
					pressedButtons.add(org.lwjgl.input.Mouse.getEventButton());
				else
					releasedButtons.add(org.lwjgl.input.Mouse.getEventButton());
			}
		}
		else
			try{org.lwjgl.input.Mouse.create();}catch(Exception e){}*/
	}

	public static boolean isButtonDown(MouseButton button)
	{
		//return org.lwjgl.input.Mouse.isButtonDown(button.id());
		return GLFW.glfwGetMouseButton(Display.getGlfwWindowHandle(), button.id()) == 1;
	}
	
	public static boolean wasButtonClicked(MouseButton button)
	{
		if(pressedButtons == null) return false;
		for(int i=0;i<pressedButtons.size();i++)
			if(pressedButtons.get(i) == button.id()) return true;
		return false;
	}
	
	public static boolean wasButtonReleased(MouseButton button)
	{
		if(releasedButtons == null) return false;
		for(int i=0;i<releasedButtons.size();i++)
			if(releasedButtons.get(i) == button.id()) return true;
		return false;
	}
	
	public static int wheelDelta()
	{
		return scrollWheelDelta;
	}
	
	public static Vec2i posInWindow()
	{
		Vec2i windowSize = Display.getSize();
		GLFW.glfwGetCursorPos(Display.getGlfwWindowHandle(), posX, posY);
		return new Vec2i((int)Math.floor(posX.get(0)), windowSize.y-(int)Math.floor(posY.get(0)));
	}
	
	public static Vec2f posInScreen(Screen screen)
	{
		Vec4f screenAspect = screen.getVisibleArea(Display.getSize());
		Vec4i pixelBounds = screen.getViewportCoords(Display.getSize());
		//Vec2f boundingScreenCoords = screen.getBoundingScreenCoords(displaySize)
		Vec2i posInWindow = posInWindow();
		return new Vec2f(
				((float)(posInWindow.x-pixelBounds.x)/(float)pixelBounds.z) * screenAspect.z + screenAspect.x,
				((float)(posInWindow.y-pixelBounds.y)/(float)pixelBounds.w) * screenAspect.w + screenAspect.y);
	}
	
	public static Vec2f posInScreen()
	{
		return posInScreen(Core.currentScreen());
	}
	
	public static boolean insideScreenArea(Vec4f area)
	{
		Vec2f pos = posInScreen();
		return 
				pos.x >= area.x && pos.y >= area.y && 
				pos.x <= area.x+area.z && pos.y <= area.y+area.w;
	}
	
	public static boolean insideWindow()
	{
		//TODO
		return true;//org.lwjgl.input.Mouse.isInsideWindow();
	}
}
