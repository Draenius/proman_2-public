package proman.texture;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.GL_GENERATE_MIPMAP;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;

import proman.core.ManagerException;
import proman.math.vector.Vec2i;
import proman.util.io.Address;

public class Sampler2D 
{
	/** Do not touch manually!*/
	@Deprecated
	public int id;
	
	/** Tries to load a new sampler from the given image file adress*/
	public Sampler2D(Address adress) throws ManagerException
	{
		if(adress == null)
		{
			this.id = 0;
			return;
		}
		
		BufferedImage image;
		try
		{
			InputStream input = adress.openInputStream();
			image = ImageIO.read(input);
			input.close();
		} catch(Exception e)
		{
			throw new ManagerException("Error loading sampler: "+e.getLocalizedMessage());
		}
		if(image == null)
			throw new ManagerException("Error loading sampler: Unable to load image file \""+adress.getPath()+"\"");
		
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);

		for(int y = 0; y < image.getHeight(); y++)
		{
			for(int x = 0; x < image.getWidth(); x++)
			{
				int pixel = pixels[y * image.getWidth() + x];
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) (pixel & 0xFF));
				buffer.put((byte) ((pixel >> 24) & 0xFF));
			}
		}

		buffer.flip();
		
		int textureID = glGenTextures();
		
		glBindTexture(GL_TEXTURE_2D, textureID);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		this.id = textureID;
		
		this.dimensions = new Vec2i(image.getWidth(), image.getHeight());
	}
	
	/** Binds the sampler with parameter "GL_NEAREST".
	*	Use it for low-pixel textures.*/
	public void bindDetailed()
	{
		if(this.id != 0)
		{
			glBindTexture(GL_TEXTURE_2D, this.id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}
		else
			glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	/** Binds the sampler with parameter "GL_LINEAR".
	* 	Use it for anti-aliased textures.*/
	public void bindSmoothed()
	{
		if(this.id != 0)
		{
			glBindTexture(GL_TEXTURE_2D, this.id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		else
			glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	
	/** Disposes the stored sampler data*/
	public void dispose()
	{
		glDeleteTextures(this.id);
		this.id = 0;
	}
	
	
	/** Returns whether the sampler texture is already and still usable*/
	public boolean isAvailable()
	{
		return this.id > 0;
	}
	
	/** Usage not recommended until not avoidable. Returns the internal handle id of the sampler.*/
	@Deprecated
	public int getID()
	{
		return this.id;
	}
	
	
	
	protected Vec2i dimensions;
	public Vec2i dimensions()
	{
		return this.dimensions.clone();
	}
	
	
	
	
	
	/** Releases any bound Sampler2D*/
	public static void releaseAny()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}
