package proman.texture;

import proman.math.vector.Vec2f;
import proman.math.vector.Vec4f;

public class Texture 
{
	Sampler2D sampler;
	Vec2f[] texCoords;
	boolean antiAliased;
	
	public Texture(Sampler2D sampler, Vec2f[] texCoords, boolean antiAliased)
	{
		this.sampler = sampler;
		this.texCoords = texCoords;
		this.antiAliased = antiAliased;
	}
	
	public Texture(Sampler2D sampler, float[][] texCoords, boolean antiAliased)
	{
		this.sampler = sampler;
		this.texCoords = new Vec2f[texCoords.length];
		
		for(int i=0;i<texCoords.length;i++)
			this.texCoords[i] = new Vec2f(texCoords[i][0], texCoords[i][1]);
		
		this.antiAliased = antiAliased;
	}
	
	public Texture(Sampler2D sampler, Vec4f excerpt, boolean antiAliased)
	{
		this.sampler = sampler;
		this.texCoords = new Vec2f[]
		{
			new Vec2f(excerpt.x, 			excerpt.y),
			new Vec2f(excerpt.x+excerpt.z, 	excerpt.y),
			new Vec2f(excerpt.x+excerpt.z, 	excerpt.y+excerpt.w),
			new Vec2f(excerpt.x, 			excerpt.y+excerpt.w)
		};
		this.antiAliased = antiAliased;
	}
	
	public Texture(Sampler2D sampler, boolean antiAliased)
	{
		this.sampler = sampler;
		this.texCoords = new Vec2f[]
		{
			new Vec2f(0, 1), new Vec2f(1, 1), new Vec2f(1, 0), new Vec2f(0, 0)
		};
		this.antiAliased = antiAliased;
	}
	
	
	public Vec2f[] getTextureExcerpts()
	{
		Vec2f[] vecs = new Vec2f[this.texCoords.length];
		for(int i=0;i<this.texCoords.length;i++)
			vecs[i] = new Vec2f(this.texCoords[i].x, this.texCoords[i].y);
		return vecs;
	}
	
	public Sampler2D getSampler()
	{
		return this.sampler;
	}
	
	
	public void bind()
	{
		if(this.antiAliased)
			this.sampler.bindSmoothed();
		else
			this.sampler.bindDetailed();
	}
}
