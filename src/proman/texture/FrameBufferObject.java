package proman.texture;

import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;

import proman.core.Core;
import proman.core.ManagerException;
import proman.math.vector.Vec2i;
import proman.math.vector.Vec4f;
import proman.math.vector.Vec4i;
import proman.rendering.Screen;

public class FrameBufferObject extends Sampler2D
{
	/** Do not touch manually!*/
	@Deprecated
	public int fboId;
	
	/** Do not touch manually!*/
	@Deprecated
	int rboDepthBufferId;
	
	Vec2i res;
	
	
	
	/** Initializes a new frame buffer object instance*/
	@SuppressWarnings("deprecation")
	public FrameBufferObject(Vec2i res) throws ManagerException
	{
		super(null);
		if(res.x < 0 || res.y < 0) throw new ManagerException("Trying to use negative framebuffer size");
		this.res = res.clone();
		
		int fboId = glGenFramebuffers();
		int texId = glGenTextures();
		int rboDepthBufferId = glGenRenderbuffers();

		glBindFramebuffer(GL_FRAMEBUFFER, fboId);
		
		glBindTexture(GL_TEXTURE_2D, texId);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, res.x, res.y, 0, GL_RGBA, GL_INT, (ByteBuffer) null);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId, 0);
		
		glBindRenderbuffer(GL_RENDERBUFFER, rboDepthBufferId);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, res.x, res.y);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepthBufferId);
		
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
		{
			this.fboId = fboId;
			this.rboDepthBufferId = rboDepthBufferId;
			
			this.id = texId;
		}
		else
			throw new ManagerException("Unable to create framebuffer");

		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		
		this.dimensions = res.clone();
	}
	
	
	
	/** Binds the FBO as the current rendering context*/
	public void bindToRender(Screen screen)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, this.fboId);
		glClear(GL_COLOR_BUFFER_BIT);
		
		glMatrixMode(GL_PROJECTION);
		GL11.glLoadIdentity();
		Vec4i screenCoord = screen.getViewportCoords(this.res);
		glViewport(screenCoord.x, screenCoord.y, screenCoord.z, screenCoord.w);
		glMatrixMode(GL_MODELVIEW);
		
		glMatrixMode(GL_PROJECTION);
		GL11.glLoadIdentity();
		Vec4f sC = screen.getVisibleArea(this.res);
		glOrtho(sC.x, sC.x+sC.z, sC.y, sC.y+sC.w, -1, 1);
		glMatrixMode(GL_MODELVIEW);
		
	}
	
	
	/** Disposes the frame buffer object*/
	@SuppressWarnings("deprecation")
	public void dispose()
	{
		glDeleteFramebuffers(this.fboId);
		glDeleteRenderbuffers(this.rboDepthBufferId);
		glDeleteTextures(this.id);
	}
	
	
	/** Releases any currently bound FBO*/
	public static void releaseAny()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		Core.currentScreen().setAsCurrent();
	}
}
