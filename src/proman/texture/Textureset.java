package proman.texture;

import java.util.HashMap;


public abstract class Textureset 
{
	
	
	private static HashMap<String, Texture> textures = new HashMap<String, Texture>();
	
	
	public static void addTexture(Texture texture, String id)
	{
		textures.put(id, texture);
	}
	
	public static Texture get(String id)
	{
		return textures.get(id);
	}
	
}
