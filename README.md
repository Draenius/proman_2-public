# README #

Dies ist eine Version meiner privaten Standardbibliothek für gering- bis mittelkomplexe Spiele 
und einfache Grafikanwendungen auf Java-Basis.

Sie abstrahiert verschiedene Funktionen der [Lightweight Java Game Library](https://lwjgl.org) sowie der
Java-Standardbibliothek zur vereinfachten persönlichen Nutzung.

Ein Großteil der implementierten OpenGL-Funktionen basiert aus Kompatibilitätsgründen momentan noch 
auf der veralteten Version OpenGL1.1.